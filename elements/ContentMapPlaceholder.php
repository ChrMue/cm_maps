<?php


namespace ChrMue\cm_Maps;


class ContentMapPlaceholder extends \ContentElement
{

	public $mapID;
	public $mapstyle;
	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'mod_cm_googlemaps_map';


	/**
	 * Generate the content element
	 */
	protected function compile()
	{
		if (TL_MODE == 'BE')
		{
			$this->strTemplate = 'be_wildcard';

			/** @var \BackendTemplate|object $objTemplate */
			$objTemplate = new \BackendTemplate($this->strTemplate);

			$this->Template = $objTemplate;
			$this->Template->title = '>>>Map<<< Placeholder';
		}
		if (TL_MODE == 'FE')
		{
			$this->Template->mapID  = $this -> mapid;
			$this->Template->mapstyle = "height:400px;";
			$this->Template->acceptanceRequired=$this->acceptanceRequired;
			$this->Template->acceptanceText=$this->acceptanceText;
		}
	}
}
