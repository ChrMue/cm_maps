<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package cm_GoogleMaps
 * @link    
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace ChrMue\cm_Maps;


class cm_CoordPicker extends \Widget
{

	/**
	 * Submit user input
	 * @var boolean
	 */
	protected $blnSubmitInput = true;

	/**
	 * Submit user input
	 * @var boolean
	 */
	protected $zoom = 10;

	/**
	 * Path nodes
	 * @var array
	 */
	protected $strName = 'Select a position';

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'be_widget';

	private $api=null;
	private $apiKey =null;

	/**
	 * Load the database object
	 * @param array
	 */
	public function __construct($arrAttributes=null)
	{
		parent::__construct($arrAttributes);
	}

	public function generateLabel()
	{
		parent::generateLabel();
	}

	private function getApiAndKey()
	{
	    //$gcapi=null;
	    //$gcapiKey=null;
	    $api=null;
	    $apiKey = null;
	    $root_id=null;
	    $root_details=null;
	    
	    if (TL_MODE == 'FE')
	    {
	        $root_id = self::getRootPageFromUrl()->id;
	        $root_details =  \PageModel::findWithDetails($root_id);
	        //$gcapi = $root_details->cm_gc_api;
	        $api = $root_details->cm_map_api;
	    }
	    //if (!$gcapi) { $gcapi=\Config::get('cm_gc_api'); }
	    //if (!$gcapi) throw new \Exception("Api not selected");
	    if (!$api) { $api=\Config::get('cm_map_api'); }
	    if (!$api) throw new \Exception("Api not selected");
	    //$gcapiprop="cm_map_apikey_".strtolower($gcapi);
	    $apiprop="cm_map_apikey_".strtolower($api);
	    
	    if (TL_MODE == 'FE')
	    {
	        //$gcapiKey = $root_details->$gcapiprop;
	        $apiKey = $root_details->$apiprop;
	    }
	    //if (!$gcapiKey) { $gcapiKey=\Config::get($gcapiprop); }
	    if (!$apiKey) { $apiKey=\Config::get($apiprop); }
	    
	    //$this->gcapi=$gcapi;
	    //$this->gcapiKey=$gcapiKey;
	    $this->api=$api;
	    $this->apiKey=$apiKey;
	}

	/**
	 * Generate the widget and return it as string
	 * @return string
	 */
	public function generate()
	{
		$this->import('BackendUser', 'User');
		
		
		$map_apikey=null;
	    $this->getApiAndKey();

		$this->useSSL = \Config::get('cm_request_gm_ssl') || \Environment::get('ssl');
		$html= '<script src="'.cm_Map_lib::getBaseScript($this->useSSL,'de',$this->api,$this->apiKey).'"></script>';
		return $html;
		$tree = '';
		$this->getPathNodes();
		$for = $this->Session->get('page_selector_search');
		$arrIds = array();

		// Return the tree
		return '<ul class="tl_listing tree_view picker_selector'.(($this->strClass != '') ? ' ' . $this->strClass : '').'" id="'.$this->strId
			.'"><li class="tl_folder_top"><div class="tl_left">'
	    	.\Image::getHtml($GLOBALS['TL_DCA'][$this->strTable]['list']['sorting']['icon'] ?: 'pagemounts.gif').' '.($GLOBALS['TL_CONFIG']['websiteTitle'] ?: 'Contao Open Source CMS')
	    	.'</div> <div class="tl_right">&nbsp;</div><div style="clear:both"></div></li><li class="parent" id="'.$this->strId.'_parent"><ul>'.$tree.$strReset
	    	.'</ul></li></ul>';
	}

}
