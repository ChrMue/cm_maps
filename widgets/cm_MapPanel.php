<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2013 Leo Feyer
 *
 * @package Core
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace Contao;

define('TL_MODE', 'BE');
// Set the script name
define('TL_SCRIPT', 'system/modules/mymod/classes/getContent.php');
require_once '../../../../system/initialize.php';
//require_once Path::join($this->projectDir, 'system/initialize.php');

/**
 * Class cm_MapPanel
 *
 * Provide methods to handle input field "file tree".
 * @copyright  Leo Feyer 2005-2013
 * @author     Leo Feyer <https://contao.org>
 * @package    Core
 */
class cm_MapPanel extends \Backend
{

	/**
	 * Current Ajax object
	 * @var object
	 */
	protected $objAjax;


	/**
	 * Initialize the controller
	 *
	 * 1. Import the user
	 * 2. Call the parent constructor
	 * 3. Authenticate the user
	 * 4. Load the language files
	 * DO NOT CHANGE THIS ORDER!
	 */
	public function __construct()
	{
		$this->import('BackendUser', 'User');
		parent::__construct();

		$this->User->authenticate();
		System::loadLanguageFile('default');
                
	}


	/**
	 * Run the controller and parse the template
	 */
	public function run()
	{
	    $BEonly = false;
	    $root_details = null;
	    $api = null;
	    $apiKey = null;
	    $tmlName = 'be_osm_mapwidget';
	    $isFE=!$BEonly &&  (TL_MODE == 'FE');
	    if ($isFE)
	    {
	        $root_id = self::getRootPageFromUrl()->id;
	        $root_details = \PageModel::findWithDetails($root_id);
	        
	        $api = $root_details->cm_map_api;
	    }
	    
	    if (!$api) { $api=\Config::get('cm_map_api'); }
	    if (!$api) throw new \Exception("Api not selected");
	    
	    //$gcapiprop="cm_map_apikey_".strtolower($gcapi);
	    switch ($api)
	    {
	        case 'GM': $tmlName = 'be_gm_mapwidget';
	           break;
	        case 'OSM': $tmlName = 'be_osm_mapwidget'; 
	           break;
	    }	    
	    $this->Template = new \BackendTemplate($tmlName);
		$this->Template->main = '';


		// Ajax request
		if ($_POST && \Environment::get('isAjaxRequest'))
		{
			$this->objAjax = new \Ajax(\Input::post('action'));
			$this->objAjax->executePreActions();
		}

		$strTable = \Input::get('table');
		$strField = \Input::get('field');

		// Define the current ID
		define('CURRENT_ID', (\Input::get('table') ? $this->Session->get('CURRENT_ID') : \Input::get('id')));

		$this->loadDataContainer($strTable);
		$objDca = new \DC_Table($strTable);

		// AJAX request
		if ($_POST && \Environment::get('isAjaxRequest'))
		{
			$this->objAjax->executePostActions($objDca);
		}

		$this->Session->set('coordPickerRef', \Environment::get('request'));

		// Prepare the widget
		$objMapPanel = new $GLOBALS['BE_FFL']['cm_CoordPicker'](array(
		
			'zoom'     => 20,
			'strName'  => 'cm_map_centerlist2', //'MyName',
//			'varValue' => explode(',', Input::get('value'))
			'varValue' => \Input::get('value')
			
		), $objDca);

		$this->Template->main = $objMapPanel->generate();
		$this->Template->theme = \Backend::getTheme();
		$this->Template->base = \Environment::get('base');
		$this->Template->language = $GLOBALS['TL_LANGUAGE'];
		$this->Template->title = specialchars($GLOBALS['TL_LANG']['MSC']['coordpicker'] ?? '');
		$this->Template->charset = $GLOBALS['TL_CONFIG']['characterSet'];
		$this->Template->addSearch = false;
		$this->Template->search = $GLOBALS['TL_LANG']['MSC']['search'];
		$this->Template->action = ampersand(\Environment::get('request'));
		$this->Template->value = $this->Session->get('location_search');
//		$this->Template->managerHref = 'contao/main.php?do=page&amp;popup=1';
		$this->Template->managerHref = 'xxx';
		$this->Template->lblLat = $GLOBALS['TL_LANG']['MSC']['cm_mapLat'];
		$this->Template->lblLng = $GLOBALS['TL_LANG']['MSC']['cm_mapLng'];
		
		
		$arr = explode(',', \Input::get('value'));
		$this->Template->lat = $arr[0];
		$this->Template->lng = $arr[1];
		
		//		$this->Template->breadcrumb = $GLOBALS['TL_DCA']['tl_page']['list']['sorting']['breadcrumb'];

		$GLOBALS['TL_CONFIG']['debugMode'] = false;
		$this->Template->output();
		//echo "xxxx";
	}
}


/**
 * Instantiate the controller
 */
$objMapPanel = new cm_MapPanel();
$objMapPanel->run();
