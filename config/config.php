<?php

/**
 * PHP version 5
 * @copyright  Christian Muenster 2020 
 * @author     Christian Muenster 
 * @package    CM_Maps
 * @license    LGPL 
 */


/**
 * Back end wizards
 */
$GLOBALS['BE_FFL']['cm_SortWizard'] = 'ChrMue\cm_Maps\cm_SortWizard';
$GLOBALS['BE_FFL']['cm_ListWizard'] = 'ChrMue\cm_Maps\cm_ListWizard';


/**
 * uses:
 * https://github.com/pointhi/leaflet-color-markers
 * 
 */

array_insert($GLOBALS['BE_MOD']['design'], 2, array
(
	'cm_mapstyles' => array
	(
  		'tables'    => array('tl_cm_gmaplayout','tl_cm_gmaptypestyle'),
		'icon'      => 'system/modules/cm_maps/assets/mapstyleicon.gif'
	),
	'cm_mapclusterstyles' => array
	(
  		'tables'    => array('tl_cm_gmapclusterlayout','tl_cm_gmapclusterstyle'),
		'icon'      => 'system/modules/cm_maps/assets/clusterstyleicon.gif'
	)
));
if (TL_MODE == 'FE')
{
    //$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/cm_maps/assets/markerclusterer.js';
    //$GLOBALS['TL_JAVASCRIPT'][] = 'https://rawgit.com/googlemaps/js-marker-clusterer/gh-pages/src/markerclusterer.js';
} 
if (TL_MODE == 'BE')
{
	$GLOBALS['TL_JAVASCRIPT'][] = 'system/modules/cm_maps/assets/coordPicker.js'; 
	$GLOBALS['TL_CSS'][] = 'system/modules/cm_maps/assets/cm_googlemapsBE.css'; 
}

$GLOBALS['TL_MODELS']['tl_cm_gmapclusterstyle']  = 'ChrMue\cm_Maps\GoogleMapsClusterStyleModel';
$GLOBALS['TL_MODELS']['tl_cm_gmapclusterlayout'] = 'ChrMue\cm_Maps\GoogleMapsClusterLayoutModel';

$GLOBALS['BE_FFL']['cm_LatLng'] 	 = 'ChrMue\cm_Maps\cm_LatLng';
$GLOBALS['BE_FFL']['cm_CoordPicker'] = 'ChrMue\cm_Maps\cm_CoordPicker';
$GLOBALS['BE_FFL']['cm_AnchorField'] = 'ChrMue\cm_Maps\cm_AnchorField';

$GLOBALS['TL_HOOKS']['executePostActions'][] = array('cm_Maps\cm_LatLngAjax', 'storePickedCoords');

$GLOBALS['TL_HOOKS']['addCustomRegexp'][] = array('cm_Maps\cm_Map_lib', 'addGeoCoordsRegexp');
$GLOBALS['TL_HOOKS']['addCustomRegexp'][] = array('cm_Maps\cm_Map_lib', 'addIntvalsAndDefaultRegexp');

$GLOBALS['TL_CTE']['includes']['cm_mapPlaceholder']='ChrMue\cm_Maps\ContentMapPlaceholder';

$GLOBALS['cm_RegisteredApi']['GM'] = 'ChrMue\cm_Maps\Api_GM';
$GLOBALS['cm_RegisteredApi']['OSM_nominatim'] = 'ChrMue\cm_Maps\Api_OSM_nominatim';
$GLOBALS['cm_RegisteredApi']['OSM_mapsco'] = 'ChrMue\cm_Maps\Api_OSM_mapsco';
$GLOBALS['cm_RegisteredApi']['OSM_mapquest'] = 'ChrMue\cm_Maps\Api_OSM_mapquest';

