<?php

/**
 * Contao Open Source CMS
 * 
* @copyright  Christian Muenster 2009-2020
 * 
 * @package cm_maps
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'ChrMue',
));  


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
    'ChrMue\cm_Maps\ApiConnectionFactory'   => 'system/modules/cm_maps/classes/base.php',
    'ChrMue\cm_Maps\Api_OSM'                => 'system/modules/cm_maps/classes/Api_OSM.php',
    'ChrMue\cm_Maps\Api_GM'                 => 'system/modules/cm_maps/classes/Api_GM.php',
    'ChrMue\cm_Maps\ApiParameter'           => 'system/modules/cm_maps/classes/API_interface.php',
    'ChrMue\cm_Maps\API_adapter'            => 'system/modules/cm_maps/classes/API_interface.php',
    //'ChrMue\cm_Maps\UpgradeHandler'         => 'system/modules/cm_maps/classes/UpgradeHandler.php',
    'ChrMue\cm_Maps\cm_SortWizard'          => 'system/modules/cm_maps/classes/cm_SortWizard.php',
    'ChrMue\cm_Maps\cm_ListWizard'          => 'system/modules/cm_maps/classes/cm_ListWizard.php',
    
    
	// Models
	'ChrMue\cm_Maps\GoogleMapsClusterLayoutModel' => 'system/modules/cm_maps/models/GoogleMapsClusterLayoutModel.php',
	'ChrMue\cm_Maps\GoogleMapsClusterStyleModel'  => 'system/modules/cm_maps/models/GoogleMapsClusterStyleModel.php',
	'ChrMue\cm_Maps\GoogleMapsMapLayoutModel'  => 'system/modules/cm_maps/models/GoogleMapsMapLayoutModel.php',
		// Modules
	// Classes
  	'ChrMue\cm_Maps\cm_Map_lib'    => 'system/modules/cm_maps/classes/cm_Map_lib.php',
  	'ChrMue\cm_Maps\Helper'            => 'system/modules/cm_maps/classes/Helper.php',
	'ChrMue\cm_Maps\ClusterLayout'       => 'system/modules/cm_maps/classes/ClusterLayout.php',
	'ChrMue\cm_Maps\Map'           => 'system/modules/cm_maps/classes/Map.php',
	'ChrMue\cm_Maps\MapLayout'           => 'system/modules/cm_maps/classes/MapLayout.php',
	'ChrMue\cm_Maps\cm_googlemaps_lib'   => 'system/modules/cm_maps/classes/cm_googlemaps_lib.php',

  	'ChrMue\cm_Maps\cm_mgm_fSockOpenConnect' => 'system/modules/cm_maps/classes/base.php',
  	'ChrMue\cm_Maps\cm_mgm_cURLConnect' => 'system/modules/cm_maps/classes/base.php',
//  	'ChrMue\cm_Maps\GoogleMapsXMLData' => 'system/modules/cm_maps/classes/base.php',

	'ChrMue\cm_Maps\ContentMapPlaceholder'  => 'system/modules/cm_maps/elements/ContentMapPlaceholder.php',
  	'ChrMue\cm_Maps\cm_LatLngAjax'	=> 'system/modules/cm_maps/classes/cm_LatLngAjax.php',
	// Widgets
	'ChrMue\cm_Maps\cm_MapPanel'		=> 'system/modules/cm_maps/widgets/cm_MapPanel.php',
	'ChrMue\cm_Maps\cm_CoordPicker'	=> 'system/modules/cm_maps/widgets/cm_CoordPicker.php',
	'ChrMue\cm_Maps\cm_LatLng'		=> 'system/modules/cm_maps/widgets/cm_LatLng.php',
    'ChrMue\cm_Maps\cm_AnchorField'          => 'system/modules/cm_maps/widgets/cm_AnchorField.php',
    
));

/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
    'cm_googlemap_js_main'          	=> 'system/modules/cm_maps/templates',
    'cm_osm_js_main'          	        => 'system/modules/cm_maps/templates',
    'mod_cm_googlemaps_map'  			=> 'system/modules/cm_maps/templates',
	'be_gm_mapwidget'                 	=> 'system/modules/cm_maps/templates',
    'be_osm_mapwidget'                 	=> 'system/modules/cm_maps/templates',
));
