<?php

/**
 * Extension for the Contao Open Source CMS
 *
 * PHP version 5
 * @copyright  Christian Muenster 2009-2018
 * @author     Christian Muenster
 * @package    CM_GoogleMaps
 * @license    LGPL
 */
/**
 * palette for tl_page
 */

/**
 * palette for tl_page
 */
$GLOBALS['TL_DCA']['tl_page']['palettes']['__selector__'][]  = 'cm_gm_acceptance_required';

$GLOBALS['TL_DCA']['tl_page']['palettes']['root'] .= ';{cm_maps_legend},cm_map_api,cm_gc_api,cm_map_apikey_gm,cm_map_apikey_osm;{cm_gm_accepance_legend:hide},cm_gm_acceptance_required,cm_cookieid_gm,cm_cookieid_osm';
$GLOBALS['TL_DCA']['tl_page']['palettes']['rootfallback'] .= ';{cm_maps_legend},cm_map_api,cm_gc_api,cm_map_apikey_gm,cm_map_apikey_osm;{cm_gm_accepance_legend:hide},cm_gm_acceptance_required,cm_cookieid_gm,cm_cookieid_osm';

$GLOBALS['TL_DCA']['tl_page']['subpalettes']['cm_gm_acceptance_required'] ='cm_gm_acceptance_text';

$GLOBALS['TL_DCA']['tl_page']['config']['onload_callback'][] = array('tl_page_cm_maps', 'modifyPalette');




/**
 * Add fields to tl_page
 */
$GLOBALS['TL_DCA']['tl_page']['fields']['cm_map_api'] = array(
    'label'                 => &$GLOBALS['TL_LANG']['tl_page']['cm_map_api'],
    'inputType'             => 'select',
    'default'               => '',
    'options'               => array
    (
        ''=> 'default',
        'GM'=>'cm_api_gm',
        'OSM'=>'cm_api_osm'
    ),
    'reference'             => &$GLOBALS['TL_LANG']['tl_page'],
    'search'                => false,
    'eval'                  => array('includeBlankOption'=>false,'tl_class'=>'w50'),
    'sql'                   => "varchar(255) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_page']['fields']['cm_gc_api'] = array(
    'label'                 => &$GLOBALS['TL_LANG']['tl_page']['cm_gc_api'],
    'inputType'             => 'select',
    'default'               => '',
    'options'               => array
    (
        ''=> 'default',
        'GM'=>'cm_api_gm',
        'OSM_nominatim'=>'cm_api_osm_nominatim',
        'OSM_mapsco'=>'cm_api_osm_mapsco',
        'OSM_mapquest'=>'cm_api_osm_mapquest'
    ),
    'reference'             => &$GLOBALS['TL_LANG']['tl_page'],
    'search'                => false,
    'eval'                  => array('includeBlankOption'=>false,'tl_class'=>'w50'),
    'sql'                   => "varchar(255) NOT NULL default ''"
);


$GLOBALS['TL_DCA']['tl_page']['fields']['cm_map_apikey_gm'] = array (
    'label' => &$GLOBALS['TL_LANG']['tl_page']['cm_map_apikey_gm'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => array('mandatory' => false,'maxlength' => 255,'tl_class'=>'w50 long'),
    'sql' => "varchar(255) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_page']['fields']['cm_map_apikey_osm'] = array (
    'label' => &$GLOBALS['TL_LANG']['tl_page']['cm_map_apikey_osm'],
    'exclude' => true,
    'inputType' => 'text',
    'eval' => array('mandatory' => false,'maxlength' => 255,'tl_class'=>'w50 long'),
    'sql' => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_page']['fields']['cm_gm_acceptance_required'] = array (
    'label'     => &$GLOBALS['TL_LANG']['tl_page']['cm_gm_acceptance_required'],
    'exclude'   => true,
    'default'   => false,
    'inputType' => 'checkbox',
    'default'   => '',
    'eval'      => array('submitOnChange' => true, 'tl_class' => 'clr m12'),
    'sql'       => "char(1) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_page']['fields']['cm_gm_acceptance_text'] = array (
    'label'       => &$GLOBALS['TL_LANG']['tl_page']['cm_gm_acceptance_text'],
    'exclude'     => true,
    'search'      => true,
    'inputType'   => 'textarea',
    'eval'        => array('rte' => 'tinyMCE', 'helpwizard' => true),
    'explanation' => 'insertTags',
    'sql'         => "text NULL"
);

$GLOBALS['TL_DCA']['tl_page']['fields']['cm_acceptance_buttontext'] = array (
    'label'       => &$GLOBALS['TL_LANG']['tl_page']['cm_acceptance_buttontext'],
    'exclude'     => true,
    'search'      => true,
    'inputType'   => 'text',
    'eval'        =>  array('mandatory' => false,'maxlength' => 255,'tl_class'=>'w50 long'),
    'explanation' => 'insertTags',
    'sql'         => "varchar(255) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_page']['fields']['cm_cookieid_gm'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_page']['cm_cookieid_gm'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>false, 'rgxp'=>'natural', 'minval'=>0, 'nospace'=>true, 'tl_class'=>'w50'),
    'sql'				=> "int(10) unsigned NOT NULL default '0'"
);
$GLOBALS['TL_DCA']['tl_page']['fields']['cm_cookieid_osm'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_page']['cm_cookieid_osm'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>false, 'rgxp'=>'natural', 'minval'=>0, 'nospace'=>true, 'tl_class'=>'w50'),
    'sql'				=> "int(10) unsigned NOT NULL default '0'"
);

class tl_page_cm_maps extends tl_page
{
    public function modifyPalette()
    {
        if (\ChrMue\cm_Maps\Helper::checkCookieBarInstalled()>0)
        {
            $GLOBALS['TL_DCA']['tl_page']['palettes']['root']  = str_replace('cm_gm_acceptance_required','cm_gm_acceptance_text,cm_acceptance_buttontext',$GLOBALS['TL_DCA']['tl_page']['palettes']['root']);
            $GLOBALS['TL_DCA']['tl_page']['palettes']['rootfallback']  = str_replace('cm_gm_acceptance_required','cm_gm_acceptance_text,cm_acceptance_buttontext',$GLOBALS['TL_DCA']['tl_page']['palettes']['rootfallback']);
        }
    }
}