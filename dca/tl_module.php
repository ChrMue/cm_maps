<?php
/**
 * @copyright  Christian Muenster 2017 
 * @author     Christian Muenster 
 * @package    CM_Maps
 * @license    LGPL 
 * @filesource based on original memberlist from Leo Feyer
 */
$GLOBALS['TL_DCA']['tl_module']['palettes']['__selector__'][]  = 'cm_map_locationfixed';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_cluster'] =
     'cm_map_cluster_gridsize,cm_map_cluster_maxzoom,'
     .'cm_map_clusterlayoutid';

$GLOBALS['TL_DCA']['tl_module']['subpalettes']['cm_map_locationfixed'] =
    'cm_map_fixlocation';
 
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
  	'eval'              => array('submitOnChange'=>true,'tl_class'=>'clr') ,
    'sql'				=> "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_clusterlayoutid'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_clusterlayoutid'],
	'inputType'          => 'select',
	'foreignKey'         => 'tl_cm_gmapclusterlayout.name',
 	'eval'               => array('includeBlankOption'=>true, 'tl_class'=>'w50'),
	'sql'				=> "int(10) unsigned NOT NULL default '0'"
);


$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_gridsize'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize'],
	'inputType'			=> 'text',
    'default'			=> 20,
	'search'			=> false,
	'eval'				=> array('mandatory'=>false,  'rgxp'=>'digit', 'tl_class'=>'w50'),
    'sql'				=> "int(10) unsigned NOT NULL default '20'"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_cluster_maxzoom'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom'],
	'inputType'          => 'select',
    'default'			=> 15,
	'options'            => array
                          (
                            0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
                          ),
	'search'             => false,
	'eval'               => array('mandatory'=>false, 'tl_class'=>'w50'),
    'sql'				=> "int(2) NOT NULL default '15'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_fitToCircle'] = array (
    'label'     => &$GLOBALS['TL_LANG']['tl_module']['cm_fitToCircle'],
    'exclude'   => true,
    'default'   => false,
    'inputType' => 'checkbox',
    'default'   => '',
    'eval'      => array ('tl_class' => 'clr'),
    'sql'       => "char(1) NOT NULL default ''",
);

$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_locationfixed'] = array
(
	'label'				=> &$GLOBALS['TL_LANG']['tl_module']['cm_map_locationfixed'],
	'inputType'			=> 'checkbox',
	'search'			=> false,
	'eval'				=> array('tl_class'=>'w50 clr'),
    'eval'               => array('submitOnChange'=>true,'tl_class'=>'w50 clr'),
	'sql'				=> "char(1) NOT NULL default ''"
);
$GLOBALS['TL_DCA']['tl_module']['fields']['cm_map_fixlocation'] = array
(
	'label'              => &$GLOBALS['TL_LANG']['tl_module']['cm_map_fixlocation'],
	'inputType'          => 'cm_LatLng', //'text',
	'search'             => false,
    'eval'               => array('mandatory'=>false, 'tl_class'=>'w50 widget','rgxp'=>'geocoords'),
	'sql'				=> "varchar(64) NOT NULL default ''"
);
