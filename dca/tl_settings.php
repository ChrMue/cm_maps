<?php 
/**

 * @copyright  Christian Muenster 2021 
 * @author     Christian Muenster 
 * @package    CM_Maps
 * @license    LGPL 
 * @filesource
*/


$GLOBALS['TL_DCA']['tl_settings']['palettes']['default'] .= ';{cm_maps_legend},cm_requestlimit,cm_request_gm_ssl,cm_map_api,cm_gc_api,cm_map_apikey_gm,cm_map_apikey_osm,cm_cookieid_gm,cm_cookieid_osm'; 

/**
 * Add fields
 */
$GLOBALS['TL_DCA']['tl_settings']['fields']['cm_requestlimit'] = array(
	    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['cm_requestlimit'],
			'exclude'                 => true,
			'inputType'               => 'text',
			'eval'                    => array('mandatory'=>false, 'rgxp'=>'natural', 'minval'=>0, 'nospace'=>true, 'tl_class'=>'w50')
);

$GLOBALS['TL_DCA']['tl_settings']['fields']['cm_map_api'] = array(
    'label'                 => &$GLOBALS['TL_LANG']['tl_settings']['cm_map_api'],
    'inputType'             => 'select',
    'default'               => 'GM',
    'options'               => array
    (
        'GM'=>'cm_api_gm',
        'OSM'=>'cm_api_osm'
    ),
    'reference'             => &$GLOBALS['TL_LANG']['tl_settings'],
    'search'                => false,
    'eval'                  => array('includeBlankOption'=>false,'tl_class'=>'w50'),
);
$GLOBALS['TL_DCA']['tl_settings']['fields']['cm_gc_api'] = array(
    'label'                 => &$GLOBALS['TL_LANG']['tl_settings']['cm_gc_api'],
    'inputType'             => 'select',
    'default'               => 'GM',
    'options'               => array
    (
        'GM'=>'cm_api_gm',
        'OSM_nominatim'=>'cm_api_osm_nominatim',
        'OSM_mapsco'=>'cm_api_osm_mapsco',
        'OSM_mapquest'=>'cm_api_osm_mapquest'
    ),
    'reference'			=> &$GLOBALS['TL_LANG']['tl_settings'],
    'search'			=> false,
    'eval'			=> array('includeBlankOption'=>false,'tl_class'=>'w50'),
);
$GLOBALS['TL_DCA']['tl_settings']['fields']['cm_map_apikey_gm'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['cm_map_apikey_gm'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50 long')
);
$GLOBALS['TL_DCA']['tl_settings']['fields']['cm_map_apikey_osm'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['cm_map_apikey_osm'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50 long')
);

$GLOBALS['TL_DCA']['tl_settings']['fields']['cm_request_gm_ssl'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['cm_request_gm_ssl'],
    'exclude'                 => true,
    'default'                 => 1,
    'inputType'               => 'checkbox',
    'eval'                    => array('tl_class'=>'w50 m12')
);

$GLOBALS['TL_DCA']['tl_settings']['fields']['cm_cookieid_gm'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['cm_cookieid_gm'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>false, 'rgxp'=>'natural', 'minval'=>0, 'nospace'=>true, 'tl_class'=>'w50')
);
$GLOBALS['TL_DCA']['tl_settings']['fields']['cm_cookieid_osm'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_settings']['cm_cookieid_osm'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('mandatory'=>false, 'rgxp'=>'natural', 'minval'=>0, 'nospace'=>true, 'tl_class'=>'w50')
);
