<?php 
use Contao\StringUtil;

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2010 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  Christian Muenster 2012
 * @author     Christian Muenster
 * @package    cm_membergooglemaps
 * @license    LGPL
 * @filesource
 */


$GLOBALS['TL_DCA']['tl_cm_gmaptypestyle'] = array
(
	'config' => array
	(
		'dataContainer'               => 'Table',
		'ptable'                      => 'tl_cm_gmaplayout',
		'enableVersioning'            => true,

		'onload_callback' => array
		(
			array('tl_cm_gmaptypestyle', 'checkPermission'),
			array('tl_cm_gmaptypestyle', 'updateMapLayout')
		),

		'oncopy_callback' => array
		(
			array('tl_cm_gmaptypestyle', 'scheduleUpdate')
		),
		'oncut_callback' => array
		(
			array('tl_cm_gmaptypestyle', 'scheduleUpdate')
		),
		'ondelete_callback' => array
		(
			array('tl_cm_gmaptypestyle', 'scheduleUpdate')
		),
		'onsubmit_callback' => array
		(
			array('tl_cm_gmaptypestyle', 'scheduleUpdate'),
			array('tl_cm_gmaptypestyle', 'updateMapLayout')
		),

		'onrestore_callback' => array
		(
			array('tl_cm_gmaptypestyle', 'updateAfterRestore')
		),
		'sql' => array
		(
			'keys' => array
			(
				'id' => 'primary',
				'pid' => 'index'
			)
		)
	),

	// List
	'list' => array
	(
		'sorting' => array
		(
			'mode'                    => 4,
			'fields'                  => array('sorting'),
			'panelLayout'             => 'filter,search,limit',
			'headerFields'            => array('name','tstamp'),
//			'child_record_callback'   => array('MapLayout', 'compileDefinition')
			'child_record_callback'   => array('tl_cm_gmaptypestyle', 'listStyles'),


		),
		'global_operations' => array
		(
			'all' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				'href'                => 'act=select',
				'class'               => 'header_edit_all',
				'attributes'          => 'onclick="Backend.getScrollOffset()" accesskey="e"'
			)
		),
		'operations' => array
		(
			'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['edit'],
				'href'                => 'act=edit',
				'icon'                => 'edit.gif'
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['copy'],
				'href'                => 'act=paste&amp;mode=copy',
				'icon'                => 'copy.gif',
				'attributes'          => 'onclick="Backend.getScrollOffset()"'
			),
			'cut' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['cut'],
				'href'                => 'act=paste&amp;mode=cut',
				'icon'                => 'cut.gif',
				'attributes'          => 'onclick="Backend.getScrollOffset()"'
			),
			'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['delete'],
				'href'                => 'act=delete',
				'icon'                => 'delete.gif',
				'attributes'          => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))return false;Backend.getScrollOffset()"'
			),
			'toggle' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['toggle'],
				'icon'                => 'visible.gif',
				'attributes'          => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s,\'tl_cm_gmaptypestyle\')"',
				'button_callback'     => array('tl_cm_gmaptypestyle', 'toggleIcon')
			),
			'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['show'],
				'href'                => 'act=show',
				'icon'                => 'show.gif'
			)
		)
	),

	// Palettes
	'palettes' => array
	(
		'__selector__'      => array('all_setvisibility', 'all_sethue', 'all_setsaturation', 'all_setlightness', 'all_setgamma',
                                 'gty_setvisibility', 'gty_sethue', 'gty_setsaturation', 'gty_setlightness', 'gty_setgamma',
                                 'lbl_setvisibility', 'lbl_sethue', 'lbl_setsaturation', 'lbl_setlightness', 'lbl_setgamma'
                                ),

		'default'           => '{mapelement_legend},feature;'
      .'{cm_all_legend},all_setvisibility,all_invert_lightness,all_sethue,all_setsaturation,all_setlightness,all_setgamma;'
      .'{cm_gty_legend},gty_setvisibility,gty_invert_lightness,gty_sethue,gty_setsaturation,gty_setlightness,gty_setgamma;'
      .'{cm_lbl_legend},lbl_setvisibility,lbl_invert_lightness,lbl_sethue,lbl_setsaturation,lbl_setlightness,lbl_setgamma;'

  ),

	// Subpalettes
 	'subpalettes' => array
 	(
    'all_setvisibility'   => 'all_visibility', 
    'all_sethue'          => 'all_hue', 
    'all_setsaturation'   => 'all_saturation', 
    'all_setlightness'    => 'all_lightness', 
    'all_setgamma'        => 'all_gamma',
    'gty_setvisibility'   => 'gty_visibility', 
    'gty_sethue'          => 'gty_hue', 
    'gty_setsaturation'   => 'gty_saturation', 
    'gty_setlightness'    => 'gty_lightness', 
    'gty_setgamma'        => 'gty_gamma',
    'lbl_setvisibility'   => 'lbl_visibility', 
    'lbl_sethue'          => 'lbl_hue', 
    'lbl_setsaturation'   => 'lbl_saturation', 
    'lbl_setlightness'    => 'lbl_lightness', 
    'lbl_setgamma'        => 'lbl_gamma'
  ),
                                 
  // Fields
	'fields' => array
	(
		'id' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL auto_increment"
		),
		'pid' => array
		(
			'foreignKey'              => 'tl_cm_gmaplayout.name',
			'sql'                     => "int(10) unsigned NOT NULL default 0",
			'relation'                => array('type'=>'belongsTo', 'load'=>'lazy')
		),
		'sorting' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default 0"
		),
		'tstamp' => array
		(
			'sql'                     => "int(10) unsigned NOT NULL default 0"
		),
		'invisible' => array
		(
			'label'                   => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['invisible'],
			'sql'                     => "char(1) NOT NULL default ''"
		),

    'feature' => array
    (
    	'label'              => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['feature'],
    	'inputType'          => 'select',
    	'options'            => array
                              (
                                'administrative',
                                'administrative.country',
                                'administrative.land_parcel',
                                'administrative.locality',
                                'administrative.neighborhood',
                                'administrative.province',
                                'all',
                                'landscape',
                                'landscape.man_made',
                                'landscape.natural',
                                'poi',
                                'poi.attraction',
                                'poi.business',
                                'poi.government',
                                'poi.medical',
                                'poi.park',
                                'poi.place_of_worship',
                                'poi.school',
                                'poi.sports_complex',
                                'road',
                                'road.arterial',
                                'road.highway',
                                'road.local',
                                'transit',
                                'transit.line',
                                'transit.station',
                                'transit.station.airport',
                                'transit.station.bus',
                                'transit.station.rail',
                                'water'
                              ),
	    'reference'         => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle'],
    	'search'            => false,
		'eval'              => array('mandatory'=>false, 'tl_class'=>'clr long'),
		'sql'				=> "varchar(64) NOT NULL default ''"
    ),
    'all_setvisibility' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setvisibility'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
			
    ),
    'all_visibility' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_visibility'],
    	'inputType'         => 'select',
    	'options'           => array
                              (
                                'on',
                                'simplified',
                                'off'
                              ),
    	'search'            => false,
		'eval'              => array('mandatory'=>false, 'tl_class'=>'w50'),
		'sql'				=> "varchar(12) NOT NULL default 'on'"
    ),
    'all_invert_lightness' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_invert_lightness'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
    'all_sethue' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_sethue'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'				=> array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'all_hue' => array
	(
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_hue'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>6, 'multiple'=>false, 'size'=>1, 'isHexColor'=>true, 'colorpicker'=>true, 'decodeEntities'=>true, 'tl_class'=>'w50 wizard'),
/*
  		'wizard' 			=> array
								(
									array('tl_cm_gmaptypestyle', 'colorPicker')
								),
*/
		'sql'				=> "varchar(64) NOT NULL default ''"
	),
    'all_setsaturation' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setsaturation'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'all_saturation' => array
	(
		'label'         	=> &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_saturation'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>32, 'tl_class'=>'w50'),
		'sql'				=> "int(10) NOT NULL default 0"
	),
    'all_setlightness' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setlightness'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'all_lightness' => array
	(
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_lightness'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>32, 'tl_class'=>'w50'),
		'sql'				=> "int(10) NOT NULL default 0"
	),
    'all_setgamma' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_setgamma'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'				=> array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'all_gamma' => array
	(
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['all_gamma'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>32, 'tl_class'=>'w50'),
		'sql'				=> "int(10) unsigned NOT NULL default 0"
	),
    'gty_setvisibility' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setvisibility'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'          	=> array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
    'gty_visibility' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_visibility'],
    	'inputType'         => 'select',
    	'options'           => array
	                              (
	                                'on',
	                                'simplified',
	                                'off'
	                              ),
    	'search'			=> false,
      	'eval'              => array('mandatory'=>false),
		'sql'				=> "varchar(12) NOT NULL default 'on'"
    ),
    'gty_invert_lightness' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_invert_lightness'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
    'gty_sethue' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_sethue'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'gty_hue' => array
	(
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_hue'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>6, 'multiple'=>false, 'size'=>1, 'isHexColor'=>true, 'colorpicker'=>true, 'decodeEntities'=>true, 'tl_class'=>'w50 wizard'),
/*
		'wizard' => array
					(
						array('tl_cm_gmaptypestyle', 'colorPicker')
					) ,
*/
  		'sql'				=> "varchar(64) NOT NULL default ''"
	),
    'gty_setsaturation' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setsaturation'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'gty_saturation' => array
	(
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_saturation'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>32, 'tl_class'=>'w50'),
		'sql'				=> "int(10) NOT NULL default 0"
	),
	'gty_setlightness' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setlightness'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'gty_lightness' => array
	(
		'label'				=> &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_lightness'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>32, 'tl_class'=>'w50'),
		'sql'				=> "int(10) NOT NULL default 0"
	),
    'gty_setgamma' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_setgamma'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'gty_gamma' => array
	(
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['gty_gamma'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>32, 'tl_class'=>'w50'),
		'sql'				=> "int(10) unsigned NOT NULL default 0"
	),
    'lbl_setvisibility' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setvisibility'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
    'lbl_visibility' => array
    (
    	'label'				=> &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_visibility'],
    	'inputType'         => 'select',
    	'options'           => array
	                              (
	                                'on',
	                                'simplified',
	                                'off'
	                              ),
    	'search'            => false,
		'eval'              => array('mandatory'=>false),
		'sql'				=> "varchar(12) NOT NULL default 'on'"
    ),
    'lbl_invert_lightness' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_invert_lightness'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
    'lbl_sethue' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_sethue'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'lbl_hue' => array
	(
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_hue'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>6, 'multiple'=>false, 'size'=>1, 'isHexColor'=>true, 'colorpicker'=>true, 'decodeEntities'=>true, 'tl_class'=>'w50 wizard'),
/*
		'wizard' => array
					(
						array('tl_cm_gmaptypestyle', 'colorPicker')
					),
*/
		'sql'				=> "varchar(64) NOT NULL default ''"
	),
    'lbl_setsaturation' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setsaturation'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'lbl_saturation' => array
	(
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_saturation'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>32, 'tl_class'=>'w50'),
		'sql'				=> "int(10) NOT NULL default 0"
	),
    'lbl_setlightness' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setlightness'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'lbl_lightness' => array
	(
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_lightness'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>32, 'tl_class'=>'w50'),
		'sql'				=> "int(10) NOT NULL default 0"
	),
    'lbl_setgamma' => array
    (
    	'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_setgamma'],
    	'inputType'         => 'checkbox',
    	'search'            => false,
		'eval'              => array('submitOnChange'=>true, 'tl_class'=>'clr w50 m12'),
		'sql'				=> "char(1) NOT NULL default ''"
    ),
	'lbl_gamma' => array
	(
		'label'             => &$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle']['lbl_gamma'],
		'inputType'         => 'text',
		'eval'              => array('maxlength'=>32, 'tl_class'=>'w50'),
		'sql'				=> "int(10) unsigned NOT NULL default 0"
	)
  )


);



class tl_cm_gmaptypestyle extends Backend
{

	public function __construct()
	{
		parent::__construct();
		$this->import('BackendUser', 'User');
	}


  public function checkPermission(){
		if ($this->User->isAdmin)
		{
			return;
		}
// 		if (!$this->User->hasAccess('xxx', 'xxxxxxx'))
// 		{
// 			$this->log('Not enough permissions to access the map type style module',
//         'tl_cm_gmaptypestyle checkPermission', TL_ERROR);
// 			$this->redirect('contao/main.php?act=error');
// 		}
		
   }

	/**
	 * Check for modified style sheets and update them if necessary
	 */
	public function updateMapLayout()
	{
		$session = $this->Session->get('maplayout_updater');

		if (!is_array($session) || empty($session))
		{
			return;
		}
//    include_once TL_ROOT.'/system/modules/cm_membergooglemaps/cmMapLayout.php';
		
//    $this->import('MapLayout');
		$this->MapLayout= new ChrMue\cm_Maps\MapLayout(); 

//    $this->cmMapLayout = new cmMapLayout();
    
		foreach ($session as $id)
		{
      		$this->MapLayout->updateMapLayout($id);
//    	$this->cmMapLayout->updateMapLayout($id);
		}

		$this->Session->set('maplayout_updater', null);
	}


	/**
	 * Schedule a style sheet update
	 * 
	 * This method is triggered when a single style or multiple styles are
	 * modified (edit/editAll), duplicated (copy/copyAll), moved (cut/cutAll)
	 * or deleted (delete/deleteAll).
	 */
	public function scheduleUpdate()
	{
		// Return if there is no ID 
		if (!CURRENT_ID || $this->Input->get('act') == 'copy' || Environment::get('isAjaxRequest'))
		{
			return;
		}

		// Store the ID in the session
		$session = $this->Session->get('maplayout_updater');
		$session[] = CURRENT_ID;
		$this->Session->set('maplayout_updater', array_unique($session));
	}


	/**
	 * Return the color picker wizard
	 * @param DataContainer
	 * @return string
	public function colorPicker(DataContainer $dc)
	{
		return ' ' . $this->generateImage('pickcolor.gif', $GLOBALS['TL_LANG']['MSC']['colorpicker'], 'style="vertical-align:top;cursor:pointer" id="moo_'.$dc->field.'"') . '
  <script>
  new MooRainbow("moo_'.$dc->field.'", {
    id:"ctrl_' . $dc->field . '",
    startColor:((cl = $("ctrl_' . $dc->field . '").value.hexToRgb(true)) ? cl : [255, 0, 0]),
    imgPath:"plugins/colorpicker/images/",
    onComplete: function(color) {
      $("ctrl_' . $dc->field . '").value = color.hex.replace("#", "");
    }
  });
  </script>';
	}
	 */

	public function listStyles($row)
	{
		$code = sprintf('<div style="float:left"><strong>%s</strong>',$GLOBALS['TL_LANG']['tl_cm_gmaptypestyle'][$row['feature']]);
		$code .='<div class="cm_mapstylemaker">';
		 
		$code .=sprintf('<div><div class="square" style="background-color:#%s;border-color:#%s"></div><div class="txt">All %s</div><div>S %d L %d G %d</div></div>' 
						,$row['all_sethue']?$row['all_hue']:'fff'
						,$row['all_sethue']?'000':'fff'
						,$row['all_setvisibility']?$row['all_visibility']:'--'
						,$row['all_setsaturation']?$row['all_saturation']:'-'
						,$row['all_setlightness']?$row['all_lightness']:'-'
						,$row['all_setgamma']?$row['all_gamma']:'-'
					);
		$code .=sprintf('<div><div class="square" style="background-color:#%s;border-color:#%s"></div><div class="txt">Element %s</div><div>S %d L %d G %d</div></div>' 
						,$row['gty_sethue']?$row['gty_hue']:'fff'
						,$row['gty_sethue']?'000':'fff'
						,$row['gty_setvisibility']?$row['gty_visibility']:'--'
						,$row['gty_setsaturation']?$row['gyt_saturation']:'-'
						,$row['gty_setlightness']?$row['gty_lightness']:'-'
						,$row['gty_setgamma']?$row['gty_gamma']:'-'
					);
		$code .=sprintf('<div><div class="square" style="background-color:#%s;border-color:#%s"></div><div class="txt">Label %s</div><div>S %d L %d G %d</div></div>' 
						,$row['lbl_sethue']?$row['lbl_hue']:'fff'
						,$row['lbl_sethue']?'000':'fff'
						,$row['lbl_setvisibility']?$row['lbl_visibility']:'--'
						,$row['lbl_setsaturation']?$row['lbl_saturation']:'-'
						,$row['lbl_setlightness']?$row['lbl_lightness']:'-'
						,$row['lbl_setgamma']?$row['lbl_gamma']:'-'
					);
		$code.='</div></div>';
		return $code;
	}

  	/**
	 * Return the "toggle visibility" button
	 * @param array
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @param string
	 * @return string
	 */
	public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
	{
		if (strlen(Input::get('tid')))
		{
			$this->toggleVisibility(Input::get('tid'), (Input::get('state') == 1));
			$this->redirect($this->getReferer());
		}

		$href .= '&amp;tid='.$row['id'].'&amp;state='.$row['invisible'];

		if ($row['invisible'])
		{
			$icon = 'invisible.gif';
		}		

		return '<a href="'.$this->addToUrl($href).'" title="'.StringUtil::specialchars($title).'"'.$attributes.'>'.Image::getHtml($icon, $label, 'data-state="' . ($row['invisible'] ? 0 : 1) . '"').'</a> ';
	}


	/**
	 * Toggle the visibility of a format definition
	 * @param integer
	 * @param boolean
	 */
	public function toggleVisibility($intId, $blnVisible)
	{
		$this->createInitialVersion('tl_cm_gmaptypestyle', $intId);
	
		// Trigger the save_callback
		if (is_array($GLOBALS['TL_DCA']['tl_cm_gmaptypestyle']['fields']['invisible']['save_callback']))
		{
			foreach ($GLOBALS['TL_DCA']['tl_cm_gmaptypestyle']['fields']['invisible']['save_callback'] as $callback)
			{
				$this->import($callback[0]);
				$blnVisible = $this->$callback[0]->$callback[1]($blnVisible, $this);
			}
		}

		// Update the database
		$this->Database->prepare("UPDATE tl_cm_gmaptypestyle SET tstamp=". time() .", invisible='" . ($blnVisible ? '' : 1) . "' WHERE id=?")
					   ->execute($intId);

		$this->createNewVersion('tl_cm_gmaptypestyle', $intId);
		$this->log('A new version of record "tl_cm_gmaptypestyle.id='.$intId.'" has been created'.$this->getParentRecords('tl_cm_gmaptypestyle', $intId), 'tl_cm_gmaptypestyle toggleVisibility()', TL_GENERAL);

		// Recreate the style sheet
		$objMaplayout = $this->Database->prepare("SELECT pid FROM tl_cm_gmaptypestyle WHERE id=?")
									    ->limit(1)
									    ->execute($intId);

		if ($objMaplayout->numRows)
		{
			//$this->import('MapLayout');
			$this->MapLayout= new ChrMue\cm_Maps\MapLayout(); 
			$this->MapLayout->updateMapLayout($objMaplayout->pid);
		}
	}

}
?>