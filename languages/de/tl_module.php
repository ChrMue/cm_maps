<?php 
/**
 * 
 * Contao extension: cm_maps 
 * 
 * @copyright  Christian Muenster 2020
 * @author     Christian Muenster
 * @license    LGPL
 * @filesource
 * 
 */


$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster']['0']="Marker zusammenfassen";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster']['1']="Nah zusammenliegen Marker zusammenfassen (Clustering)";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize']['0']="Clustergröße";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_gridsize']['1']="Je größer das Raster in dem Marker zusammengefasst werden.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom']['0']="Größte Zoomstufe";
$GLOBALS['TL_LANG']['tl_module']['cm_map_cluster_maxzoom']['1']="Zoomstufe, bis zu der Marker zusammengefasst werden.";
$GLOBALS['TL_LANG']['tl_module']['cm_email_nameRequired']['0']="Angabe des Namens erforderlich";
$GLOBALS['TL_LANG']['tl_module']['cm_email_nameRequired']['1']="Sofern ein Formular zur E-Mail-Benachrichtigung des Mitglieds angezeigt wird, legen Sie hier fest, ob neben der E-Mail-Adresse auch der Name im Formular angegeben werden muss.";
$GLOBALS['TL_LANG']['tl_module']['cm_map_clusterlayoutid']['0']="Cluster-Layout";
$GLOBALS['TL_LANG']['tl_module']['cm_map_clusterlayoutid']['1']="Wählen SIe das gewünschte Clusterlayout.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_locationfixed']['0']="Feste Location verwenden";
$GLOBALS['TL_LANG']['tl_module']['cm_map_locationfixed']['1']="Eine feste Location als Ausgangsposition für die Umkreissuche verwenden.";

$GLOBALS['TL_LANG']['tl_module']['cm_map_fixlocation']['0']="Feste Location";
$GLOBALS['TL_LANG']['tl_module']['cm_map_fixlocation']['1']="Location, die als Ausgangsposition für die Umkreissuche verwenden soll.";

$GLOBALS['TL_LANG']['tl_module']['cm_fitToCircle']['0']="Auf Umkreis zoomen";
$GLOBALS['TL_LANG']['tl_module']['cm_fitToCircle']['1']="Auf Umkreis zoomen";
