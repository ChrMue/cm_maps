<?php 

/**
 * 
 * Contao extension: cm_maps 
 * 
 * @copyright  Christian Muenster 2020
 * @author     Christian Muenster
 * @license    LGPL
 * @filesource
 * 
 */


/**
 * Fields
 */
//$GLOBALS['TL_LANG']['tl_page']['cm_map_apikey']= array('Google API Key', 'Bitte geben Sie den Key, den Sie von Google unter <a href="http://code.google.com/apis/maps/signup.html">http://code.google.com/apis/maps/signup.html</a> erhalten haben.');
$GLOBALS['TL_LANG']['tl_page']['cm_maps_legend'] = "Maps Api";

$GLOBALS['TL_LANG']['tl_page']['cm_map_api']= array('Map Api', 'Wählen Sie den Anbieter der Karten-Schnittstelle.');
$GLOBALS['TL_LANG']['tl_page']['cm_gc_api']= array('Geocoding Api', 'Wählen Sie den Anbieter für das Geocoding.');
$GLOBALS['TL_LANG']['tl_page']['cm_map_apikey_gm']= array('API Key (Google)', 'Bitte geben Sie den Key an, den Sie von Google unter <a href="http://code.google.com/apis/maps/signup.html">http://code.google.com/apis/maps/signup.html</a> erhalten haben.');
$GLOBALS['TL_LANG']['tl_page']['cm_map_apikey_osm']= array('API Key (Geocoding unter OpenStreetMap)', 'Bitte geben Sie den Key an, den Sie für das Geocoding erhalten haben.');
$GLOBALS['TL_LANG']['tl_page']['cm_cookieid_gm']= array('Cookie (Google Maps)', 'Bitte geben Sie die Id der Cookiedefinition in der Cookiebar für Google Maps an (Überschreibt den Eintrag in "Einstellungen".');
$GLOBALS['TL_LANG']['tl_page']['cm_cookieid_osm']= array('Cookie (OpenStreetMap)', 'Bitte geben Sie die Id der Cookiedefinition in der Cookiebar für OpenStreetMap an (Überschreibt den Eintrag in "Einstellungen".');

$GLOBALS['TL_LANG']['tl_page']['cm_acceptance_buttontext']= array('Buttontext Kartenanzeige freigeben', 'Bitte geben Sie die Beschriftung des Buttons zur Freigabe der Kartenanzeige ein.');

$GLOBALS['TL_LANG']['tl_page']['cm_api_gm']= 'Google Maps';
$GLOBALS['TL_LANG']['tl_page']['cm_api_osm']= 'OpenStreetMap';
$GLOBALS['TL_LANG']['tl_page']['cm_api_osm_nominatim']= 'OpenStreetMap/Nominatim';
$GLOBALS['TL_LANG']['tl_page']['cm_api_osm_mapsco']= 'OpenStreetMap/Maps.co';
$GLOBALS['TL_LANG']['tl_page']['cm_api_osm_mapquest']= 'OpenStreetMap/Mapquest.com';
