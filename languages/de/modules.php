<?php 

/**
 * 
 * Contao extension: cm_maps 
 * 
 * @copyright  Christian Muenster 2020
 * @author     Christian Muenster
 * @license    LGPL
 * @filesource
 * 
 */

$GLOBALS['TL_LANG']['MOD']['cm_mapstyles'][0] = "Kartenlayout";
$GLOBALS['TL_LANG']['MOD']['cm_mapstyles'][1] = "Definieren Sie individuelle Karten-Layouts für die Verwendung in der Erweiterung cm_membergooglemaps.";
$GLOBALS['TL_LANG']['MOD']['cm_mapclusterstyles'][0] = "Markercluster-Layout";
$GLOBALS['TL_LANG']['MOD']['cm_mapclusterstyles'][1] = "Definieren Sie individuelle Layouts der Marker Clusters für die Verwendung in der Erweiterung cm_membergooglemaps.";

