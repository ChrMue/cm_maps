<?php
/**
 * 
 * Contao extension: cm_maps 
 * 
 * @copyright  Christian Muenster 2021
 * @author     Christian Muenster
 * @license    LGPL
 * @filesource
 * 
 */
 
$GLOBALS['TL_LANG']['tl_settings']['cm_maps_legend'] = "Maps Api";
$GLOBALS['TL_LANG']['tl_settings']['cm_requestlimit'][0] = "Max. Anzahl der Anfragen je Adresse";
$GLOBALS['TL_LANG']['tl_settings']['cm_requestlimit'][1] = "Bitte geben Sie an, wie of maximal versucht werden soll, zu einer -Adresse über die Api die Koordinaten zu ermitteln.";
$GLOBALS['TL_LANG']['tl_settings']['cm_request_gm_ssl'][0] = "Api-Abfragen immer mit SSL";
$GLOBALS['TL_LANG']['tl_settings']['cm_request_gm_ssl'][1] = "Bitte geben Sie an, ob Api-Abfragen immer mit ssl erfolgen sollen.";
$GLOBALS['TL_LANG']['tl_settings']['cm_map_api']= array('Map Api', 'Wählen Sie den Anbieter der Karten-Schnittstelle.');
$GLOBALS['TL_LANG']['tl_settings']['cm_gc_api']= array('Geocoding Api', 'Wählen Sie den Anbieter für das Geocoding.');
$GLOBALS['TL_LANG']['tl_settings']['cm_map_apikey_gm']= array('API Key (Google)', 'Bitte geben Sie den Key an, den Sie von Google unter <a href="http://code.google.com/apis/maps/signup.html">http://code.google.com/apis/maps/signup.html</a> erhalten haben.');
$GLOBALS['TL_LANG']['tl_settings']['cm_map_apikey_osm']= array('API Key (OpenStreetMap)', 'Bitte geben Sie den Key an, den Sie von OpenStreetMap erhalten haben.');
$GLOBALS['TL_LANG']['tl_settings']['cm_cookieid_gm']= array('Cookie (Google Maps)', 'Bitte geben Sie die Id der Cookiedefinition in der Cookiebar für Google Maps an.');
$GLOBALS['TL_LANG']['tl_settings']['cm_cookieid_osm']= array('Cookie (OpenStreetMap)', 'Bitte geben Sie die Id der Cookiedefinition in der Cookiebar für OpenStreetMap an.');

$GLOBALS['TL_LANG']['tl_settings']['cm_api_gm']= 'Google Maps';
$GLOBALS['TL_LANG']['tl_settings']['cm_api_osm']= 'OpenStreetMap';
$GLOBALS['TL_LANG']['tl_settings']['cm_api_osm_nominatim']= 'OpenStreetMap/Nominatim';
$GLOBALS['TL_LANG']['tl_settings']['cm_api_osm_mapsco']= 'OpenStreetMap/Maps.co';
$GLOBALS['TL_LANG']['tl_settings']['cm_api_osm_mapquest']= 'OpenStreetMap/Mapquest.com';

