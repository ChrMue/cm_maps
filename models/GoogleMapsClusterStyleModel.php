<?php

/**
 * Memberlist specific model methods
 * 
 * @package   Models
 * @author    Christian Münster
 * @copyright Christian Münster 2020
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace ChrMue\cm_Maps;


class GoogleMapsClusterStyleModel extends \Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_cm_gmapclusterstyle';

    public static function findByParent($pid)
    {
        $t = static::$strTable;
         
        $arrColumns = array("$t.pid=?","$t.invisible!=?");
        $arrValues = array($pid,1);
        $arrOptions['order'] = "$t.sorting";
         
        return static::findBy($arrColumns, $arrValues, $arrOptions);
    }
}
