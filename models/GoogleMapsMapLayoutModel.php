<?php

/**
 * Contao Open Source CMS
 * 
 * Copyright (C) 2005-2012 Leo Feyer
 * 
 * @package Core
 * @link    http://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace ChrMue\cm_Maps;


/**
 * Memberlist specific model methods
 * 
 * @package   Models
 * @author    Christian Münster
 * @copyright Christian Münster 2014
 */
class GoogleMapsMapLayoutModel extends \Model
{
	/**
	 * Table name
	 * @var string
	 */
	protected static $strTable = 'tl_cm_gmaplayout';

}
