<?php

/**
 * PHP version 5
 * @copyright  Christian Münster 2012-2022
 * @author     Christian Münster <http://www.christian-muenster.de>
 * @package    Backend
 * @license    LGPL
 * @filesource
 */


namespace ChrMue\cm_Maps;
/**
 * Class MapLayout
 *
 * Provide methods to handle the map style settings.
 * @copyright  Christian Münster 2012
 * based on classes and methods to generate css by Leo Feyer <http://www.contao.org>
 * @package    Controller
 */
class MapLayout extends \Backend
{
  const layoutFilePath = '/system/modules/cm_maps/assets';  
  const layoutFileExt = 'mst';

	/**
	 * Import String library
	 */
	public function __construct()
	{
		parent::__construct();

		if (!version_compare(VERSION, '3.5', '<'))
		{
			$this->import('StringUtil');
		}
		else
		{
			$this->import('String');
		}
//		$this->import('Files');
	}


	/**
	 * Update a Map Layout
	 * @param integer
	 */
	public function updateMapLayout($intId)
	{

		$objMapLayout = \Database::getInstance()->prepare("SELECT * FROM tl_cm_gmaplayout WHERE id=?")
										->limit(1)
										->execute($intId);

		if ($objMapLayout->numRows < 1)
		{
			return;
		}
		$layoutFileName = $objMapLayout->name;

		// Delete the map layout file
		if (\Input::get('act') == 'delete')
		{
			//$this->import('Files');
		    \Files::getInstance()->delete(self::layoutFilePath . '/' . $objMapLayout->name . self::layoutFileExt);
			
		}

		// Update the map layout file
		else
		{
			$this->writeMapLayout($objMapLayout->row());
			//$this->log('Generated map layout "' . $objMapLayout->name . '.' . self::layoutFileExt .'"', 'MapLayout updateMapLayout()', TL_CRON);
			\System::log('Generated map layout "' . $objMapLayout->name . '.' . self::layoutFileExt .'"', 'MapLayout updateMapLayout()', TL_CRON);
		}
	}


	/**
	 * Update all map layouts in the scripts folder
	 */
	public function updateMapStyles()
	{
	    $objMapStyles = \Database::getInstance()->execute("SELECT * FROM tl_cm_gmaplayout");
		$arrMapStyles = $objMapStyles->fetchEach('name');

		// Make sure the dcaconfig.php file is loaded
		@include(TL_ROOT . '/system/config/dcaconfig.php');

		// Delete old style sheets
		foreach (scan(TL_ROOT . self::layoutFilePath, true) as $file)
		{
			// Skip directories
			if (is_dir(TL_ROOT . self::layoutFilePath . '/' . $file))
			{
				continue;
			}

			// Preserve root files (is this still required now that scripts are in system/scripts?)
			if (\is_array($GLOBALS['TL_CONFIG']['rootFiles']) && in_array($file, $GLOBALS['TL_CONFIG']['rootFiles']))
			{
				continue;
			}

			// Do not delete the combined files (see #3605)
			if (preg_match('/^[a-f0-9]{12}\.' . self::layoutFileExt . '$/', $file))
			{
				continue;
			}
			$objFile = new \File(self::layoutFilePath . '/' . $file);

			// Delete the old style sheet
			if ($objFile->extension == self::layoutFileExt && !in_array($objFile->filename, $arrStyleSheets))
			{
				$objFile->delete();
			}
		}

		$objMapStyles->reset();

		// Create the new style sheets
		while ($objMapStyles->next())
		{
			$this->writeMapLayout($objMapStyles->row());
			\System::log('Generated map layout "' . $objMapStyles->name . '.' . self::layoutFileExt . '"', 'MapStyles updateMapStyles()', TL_CRON);
		}
	}


	/**
	 * Write a style sheet to a file
	 * @param array
	 */
	protected function writeMapLayout($row)
	{
		if ($row['id'] == '' || $row['name'] == '')
		{
			return;
		}
		
		//$row['name'] = basename($row['name']);
		$layoutFileName = $row['name'];
		
		//echo (self::layoutFilePath. '/' . $layoutFileName . '.' . self::layoutFileExt);
		//die();
		
		// Check whether the target file is writeable
		if (file_exists(self::layoutFilePath. '/' . $layoutFileName . '.' . self::layoutFileExt) 
			&& !$this->Files->is_writeable(self::layoutFilePath. '/' . $layoutFileName . '.' . self::layoutFileExt))
		{
			\Message::addError(sprintf($GLOBALS['TL_LANG']['ERR']['notWriteable'], self::layoutFilePath. '/' . $layoutFileName . '.' . self::layoutFileExt));
			return;
		}

		$intCount = 0;
		$vars = array();

		// Merge the global style sheet variables
		if ($row['vars'] != '')
		{
		    if ($row['vars'] != '' && \is_array(($tmp = \StringUtil::deserialize($row['vars']))))
			{
				foreach ($tmp as $v)
				{
					$vars[$v['key']] = $v['value'];
				}
			}
		}
		$this->import('Files');

		uksort($vars, 'length_sort_desc');

		$objFile = new \File(self::layoutFilePath. '/' . $layoutFileName . '.' . self::layoutFileExt);
		$objFile->write('/* Map Layout ' . $row['name'] . " */\n");
		$objFile->write('var cmMapStyle_'.$row['id'] . "= [ \n");
		$objDefinitions = \Database::getInstance()->prepare("SELECT * FROM tl_cm_gmaptypestyle WHERE pid=? AND invisible!='1' ORDER BY sorting")
										 ->execute($row['id']);

    // Append the definition
		$first=true;
    	while ($objDefinitions->next())
		{
      		$objFile->append(($first?"\n":",\n").$this->compileDefinition($objDefinitions->row(), true, $vars), '');
      		$first=false;
		}
    	$objFile->append("\n ];");
		$objFile->close();
	}

	private function getDefinition($r,$type,$style,$isColor,$styleArr,$isInt=false)
	{
		
		if ($r[$type.'_set'.$style])
			$styleArr[]= "{".$style.": "
        .($isInt||$isBool?'':'"').($isColor?"#":"").($isBool?($r[$type."_".$style]==1?'true':'false'):$r[$type."_".$style])
        .($isInt||$isBool?'':'"')."}";
    return $styleArr;
	}
 
	private function getDefinitionDirectOnTrue($r,$type,$style,$isColor,$styleArr)
	{
		if ($r[$type."_".$style]==1)
			$styleArr[]= "{".$style.": true}";
    	return $styleArr;
	}
	/**
	 * Compile format definitions and return them as string
	 * @param array
	 * @param boolean
	 * @param array
	 * @return string
	 */
	public function compileDefinition($row, $blnWriteToFile=false, $vars=array())
	{
		$return .= "{\nfeatureType: \"".$row['feature']."\"";
		$toCompile = array('all','gty','lbl');
		foreach ($toCompile as $elementType) 
		{
			$stylers="";
			$return .= ",\nelementType: \"".$elementType."\"";
      		$stylersArr=array();
			$stylersArr=$this->getDefinition($row, $elementType, 'visibility', false,$stylersArr,false);
			$stylersArr=$this->getDefinitionDirectOnTrue($row, $elementType, 'invert_lightness', false,$stylersArr);
			$stylersArr=$this->getDefinition($row, $elementType, 'hue', true,$stylersArr);
			$stylersArr=$this->getDefinition($row, $elementType, 'saturation', false,$stylersArr,true);
			$stylersArr=$this->getDefinition($row, $elementType, 'lightness', false,$stylersArr,true);
		    if (count($stylersArr)>0)
		      $return .=",\nstylers: [\n".implode(',', $stylersArr)."\n]"; 
		}
		return $return."\n}";
	}


	/**
	 * Compile a color value and return a hex or rgba color
	 * @param mixed
	 * @param boolean
	 * @param array
	 * @return string
	 */
	protected function compileColor($color, $blnWriteToFile=false, $vars=array())
	{
		if (!\is_array($color))
		{
			return '#' . $this->shortenHexColor($color);
		}
		elseif (!isset($color[1]) || empty($color[1]))
		{
			return '#' . $this->shortenHexColor($color[0]);
		}
		else
		{
			return 'rgba(' . implode(',', $this->convertHexColor($color[0], $blnWriteToFile, $vars)) . ','. ($color[1] / 100) .')';
		}
	}


	/**
	 * Try to shorten a hex color
	 * @param string
	 * @return string
	 */
	protected function shortenHexColor($color)
	{
		if ($color[0] == $color[1] && $color[2] == $color[3] && $color[4] == $color[5])
		{
			return $color[0] . $color[2] . $color[4];
		}

		return $color;
	}


	/**
	 * Convert hex colors to rgb
	 * @param string
	 * @param boolean
	 * @param array
	 * @return array
	 * @see http://de3.php.net/manual/de/function.hexdec.php#99478
	 */
	protected function convertHexColor($color, $blnWriteToFile=false, $vars=array())
	{
		// Support global variables
		if (strncmp($color, '$', 1) === 0)
		{
			if (!$blnWriteToFile)
			{
				return array($color);
			}
			else
			{
				$color = str_replace(array_keys($vars), array_values($vars), $color);
			}
		}

		$rgb = array();

		// Try to convert using bitwise operation
		if (strlen($color) == 6)
		{
			$dec = hexdec($color);
			$rgb['red'] = 0xFF & ($dec >> 0x10);
			$rgb['green'] = 0xFF & ($dec >> 0x8);
			$rgb['blue'] = 0xFF & $dec;
		}

		// Shorthand notation
		elseif (strlen($color) == 3)
		{
			$rgb['red'] = hexdec(str_repeat(substr($color, 0, 1), 2));
			$rgb['green'] = hexdec(str_repeat(substr($color, 1, 1), 2));
			$rgb['blue'] = hexdec(str_repeat(substr($color, 2, 1), 2));
		}

		return $rgb;
	}



	/**
	 * Check the name of an imported file
	 * @param string
	 * @return string
	 */
//	public function checkMapLayoutName($strName)
	public function checkStyleSheetName($strName)
	{
	    $objStyleSheet = \Database::getInstance()->prepare("SELECT COUNT(*) AS total FROM tl_cm_gmaplayout WHERE name=?")
										->limit(1)
										->execute($strName);

		if ($objStyleSheet->total < 1)
		{
			return $strName;
		}

		$chunks = explode('-', $strName);
		$i = (count($chunks) > 1) ? array_pop($chunks) : 0;
		$strName = implode('-', $chunks) . '-' . (intval($i) + 1);

		return $this->checkStyleSheetName($strName);
	}
//-----------------------------------------------------

	/**
	 * Create a format definition and insert it into the database
	 * @param array
	 */
	protected function createDefinition($arrDefinition)
	{
		$arrSet = array
		(
			'pid' => $arrDefinition['pid'],
			'sorting' => $arrDefinition['sorting'],
			'tstamp' => time(),

		);

		$arrAttributes = array_map('trim', explode(';', $arrDefinition['attributes']));

		foreach ($arrAttributes as $strDefinition)
		{
			// Skip empty definitions
			if (trim($strDefinition) == '')
			{
				continue;
			}

			$arrChunks = array_map('trim', explode(':', $strDefinition, 2));
			$strKey = strtolower($arrChunks[0]);

			$blnIsOwn = true;

			// Allow custom definitions
			if (isset($GLOBALS['TL_HOOKS']['createMapstyleDefinition']) && 
          	\is_array($GLOBALS['TL_HOOKS']['createMapstyleDefinition']))
			{
				foreach ($GLOBALS['TL_HOOKS']['createMapstyleDefinition'] as $callback)
        		{
					$this->import($callback[0]);
					$arrTemp = $this->$callback[0]->$callback[1]($strKey, $arrChunks[1], $strDefinition, $arrSet);

					if ($arrTemp && \is_array($arrTemp))
					{
						$blnIsOwn = false;
						$arrSet = array_merge($arrSet, $arrTemp);
					}
				}
			}

			// Unknown definition
			if ($blnIsOwn)
			{
				$arrSet['own'][] = $strDefinition;
			}
		}

		if (!empty($arrSet['own']))
		{
			$arrSet['own'] = implode(";\n", $arrSet['own']) . ';';
		}

		\Database::getInstance()->prepare("INSERT INTO tl_gm_gmaptypestyle %s")->set($arrSet)->execute();
	}
}
