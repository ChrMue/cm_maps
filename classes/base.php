<?php 

namespace ChrMue\cm_Maps;


/*
 * @copyright  Christian Muenster 2009 
 * @author     Christian Muenster 
 * @package    library for module CM_MemberGoogleMaps
 * @license    LGPL 
 * @filesource based on original memberlist from Leo Feyer
*/

interface Icm_connection {
    // Die Methode wird über das Interface nur vorgeschrieben,
    // daher darf sie keinen Inhalt haben.
    public function downloadFromSettings(ApiParameter $parameter);
    
    public function download($host,$ssl,$url,$moreOptions);
    public function checkInstalled();
}
 

class cm_mgm_fSockOpenConnect implements Icm_connection
{
	private function fSockOpendownload($host, $ssl, $url)
	{

    // 'http://maps.google.com/maps/geo?output=xml&key='.$gm_refid.'&oe=utf-8&q='
 
	if($ssl){
	    $host = "ssl://".$host;
            $port = 443;
	}
  	else $port = 80;
     
      $fsock = fsockopen($host, $port);
      if ($fsock) {
        fwrite($fsock, "GET " . $url . "\r\nHost: " . $host . "\r\nConnection: Close\r\n\r\n");
        $body = false;
        $data = '';
        WHILE(!feof($fsock)) {
            //$data .= fgets($fsock, 4096) . "\n";
            //$data .= fread($fsock, 1024);
          $s = fgets($fsock, 1024);
          if ( $body )
              $data .= $s;
          if ( $s == "\r\n" )
              $body = true;
        }
        fclose($fsock);
        return $data;
      }
      else
        //$this->sys->log("FAIL: fsockopen Functions","cm_membergooglemaps",TL_GENERAL);
        return null;
	}

    function checkInstalled()
    {
        return true;
    }

    function downloadFromSettings(ApiParameter $parameter)
    {     
        $fullurl=$parameter->folder
        .($parameter->subfolder?$parameter->subfolder:"")
            .'?'.http_build_query($parameter->options);
                
        return $this->fSockOpendownload($parameter->host,$parameter->useSSL,$fullurl);       
    }
	function download($host,$ssl,$url,$moreOptions)
	{
	    @trigger_error('download has been deprecated use downloadFromSettings', E_USER_DEPRECATED);
	    
//   		$fullurl=$url."kml".$moreOptions;
	    $fullurl=$url.$moreOptions;
    
	    $daten = $this->fSockOpendownload($host,$ssl,$fullurl);
	    $matches = array();
		
   		//preg_match_all('/<c o="([^"]*)" l="([^"]*)" s="([^"]*)">([^<]*)<\/c>/', $daten, $matches, PREG_SET_ORDER);
//  		preg_match_all('/^[^<]*(<kml.*<\/kml>)[^>]*$/', $daten, $matches, PREG_SET_ORDER);
//  		preg_match_all('/<coordinates>*.*<\/coordinates>/', $daten, $matches);
//  		preg_match('/<coordinates>*.*<\/coordinates>/', $daten, $matches);
      //die();
//      return $matches;
        return $daten;    
	}
}

class cm_mgm_cURLConnect implements Icm_connection
{
    
    public function cURLcheckBasicFunctions()
    {
        if( !function_exists("curl_init") ||
        !function_exists("curl_setopt") ||
        !function_exists("curl_exec") ||
        !function_exists("curl_close"))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private function cURLdownload($url,$ssl, $setExtraOptions=false)
    {
        $url = ($ssl?'https://':'http://').$url;
        $ch = curl_init();
        if($ch)
        {
            if( !curl_setopt($ch, CURLOPT_URL, $url) ) 
            {
                throw new \Exception("FAIL: curl_setopt(CURLOPT_URL)");
                return null;
            }
            if( !curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1) ) 
            { 
                throw new \Exception("FAIL: curl_setopt(CURLOPT_RETURNTRANSFER)");
                return null;
            }
            if ($setExtraOptions)
            {
                if( !curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0) ) 
                { 
                    throw new \Exception("FAIL: curl_setopt(CURLOPT_SSL_VERIFYPEER)");
                    return null;
                }
                if( !curl_setopt($ch, CURLOPT_REFERER, $url))
                { 
                    throw new \Exception("FAIL: curl_setopt(CURLOPT_REFERER)");
                    return null;
                }
                if( !curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36"))
                { 
                    throw new \Exception("FAIL: curl_setopt(CURLOPT_USERAGENT)");
                    return null;
                }
            }
            if( !curl_setopt($ch, CURLOPT_HEADER, 0) ) 
            {
                throw new \Exception("FAIL: curl_setopt(CURLOPT_HEADER)");
                return null;
            }
            $retvalue =curl_exec($ch);
            if ($retvalue === false) {
                throw new \Exception(curl_error($ch), curl_errno($ch));
            }    
            curl_close($ch);
                
            return $retvalue;
        }
        else  
        {
          throw new \Exception("FAIL: curl_init()");
          return null;
        }
    }
    
	public function checkInstalled()
    {
        return $this->cURLcheckBasicFunctions();
    }

    public function downloadFromSettings(ApiParameter $parameter)
    {
        $fullurl=$parameter->host.$parameter->folder
        .($parameter->subfolder?$parameter->subfolder:"")
        .'?'.http_build_query($parameter->options);
        return $this->cURLdownload($fullurl,$parameter->useSSL, $parameter->setExtraOptions);
    }
    public function download($host,$ssl,$url,$moreOptions)
	{
	    @trigger_error('download has been deprecated. Use downloadFromSettings', E_USER_DEPRECATED);
	    
	    if( !$this->cURLcheckBasicFunctions() ) {
		    throw new \Exception("UNAVAILABLE: cURL Basic Functions");
		    return null;
	    }
	    $fullurl=$host.$url.$moreOptions;
		$daten = $this->cURLdownload($fullurl,$ssl,$parameter->setExtraOptions);
		return $daten;
	}
}

class ApiConnectionFactory {
	private $URL_Base;
//	private $refid;
    private $accessObj;

	private $todo;
	private $ssl=false;
	
	private $apiArr = array(
	    "useSSL"   => true,
	    "host"     => 'www.mapquestapi.com',
	    "folder"   => '/geocoding/v1/address',
	    "options"  => array(
	                       "outFormat" => "",
	                       "key" => ""
	                   )
	);
	
	private $apiInterface=null;

	private function setApi(API_adapter $api)
	{
	    $this->apiInterface =$api;
	}
	
    public function setPath($host,$ssl,$path,$moreOptions)
	{
		$this->ssl= $ssl?true:false;
		//$this->URL_Base=$path."xml";
		$this->URL_Base=$path."xml";
		$this->Host=$host;
		$this->moreOptions=$moreOptions;
	}

	static function getApiConnector($api)
	{
	    
	    $connection = new cm_mgm_cURLConnect();
	    if (!$connection->cURLcheckBasicFunctions())
	    {
	        $connection = new cm_mgm_fSockOpenConnect();
	    }
	    

	    $apiClass= $GLOBALS['cm_RegisteredApi'][$api];
	    if (!$apiClass)
	    {
	        throw new \Exception("Api not registered");
	    }
	    $apiConnector = new $apiClass($connection);
	    //print_r($apiConnector); die();
	    //$apiConnector = new Api_OSM($connection);
	    return $apiConnector;
	}
	

	function readXML($Location,$country=null)
	{
	    //$tempurl= "?".($this->moreOptions ? $this->moreOptions."&" : "");
	    $tempurl= $this->moreOptions;
	    if ($Location)
	    {
	        $tempurl .= $Location.($country?",".$country:"");
	        //die($this->Host.$this->URL_Base.$tempurl);
	        	        
	        //$daten=$this->accessObj->download($this->Host,$this->ssl, $this->URL_Base,$tempurl);	        
	        //echo($this->Host . ' - '.$this->URL_Base.' - ' .$tempurl); die('### '.$Location);	        
	        
	        if ($this->apiInterface->validateResponse())
	        {
	            
	        }
	        $daten=$this->accessObj->download($this->Host,false/*$this->ssl*/, $this->URL_Base,$tempurl);
	        //die("URL: ".$tempurl);	        
	        if ($daten) { $xmlData = new \SimpleXMLElement($daten); }

	    }
	    return $xmlData;
	}
	
}

    