<?php
namespace ChrMue\cm_Maps;

class ApiParameter
{
    public $useSSL;
    public $host;
    public $folder;
    public $subfolder;
    public $options;
    public $setExtraOptions;
}

abstract class API_adapter{    

    protected $xml = null;
    protected $apiParameter;
    protected $status = false;
    protected $accessObj = null;
    
    public function getResponse()
    {
        // TODO Auto-generated method stub
        return $this->xml;
    }
    protected function setConnection(Icm_connection $connection)
    {
        $this->accessObj=$connection;
    }
    protected function getParameters()
    {
        // TODO Auto-generated method stub
        return $this->apiParameter;
    }

    abstract function validateResponse();
    
    abstract function getLat();
    abstract function getLng();
    
    abstract function setParameters($format,$ssl,$location,$countryCode,$key);
    
    protected function processRequest()
    {
        $daten=$this->accessObj->downloadFromSettings($this->getParameters());
//        print_r($this->apiParameter);
//        echo "daten:";
//        print_r($daten);die("abc");
        
        if (!$daten){
            $this->xml=null;
            return false;
        }
        $this->xml = new \SimpleXMLElement($daten);
    }
    
}