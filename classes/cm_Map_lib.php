<?php

namespace ChrMue\cm_Maps;

class cm_Map_lib {

  public static function getMapTypeStr($type)
  {
	switch($type) {
		case 'n':
			return "ROADMAP"; //"G_NORMAL_MAP";
			break;
		case 's':
			return "SATELLITE"; //"G_SATELLITE_MAP";
			break;
		case 'h':
			return "HYBRID"; //"G_HYBRID_MAP";
			break;
		case 'p':
			return "TERRAIN"; //"G_PHYSICAL_MAP";
			break;
		default:
			return "ROADMAP";
			break;
	}
  }

  public static function getCtrlTypeStr($type)
  {
	switch ($type) {
		case 'hb':
			return 'HORIZONTAL_BAR';
			break;
		case 'dm':
			return 'DROPDOWN_MENU';
			break;
		case 'df':
		default:
			return 'DEFAULT';
			break;
	}
  }

  public static function getCtrlNavStr($type)
  {
    switch ($type) {
      case 'sm':
        return 'SMALL';
        break;
      case 'zo':
        return 'ZOOM_PAN';
        break;
      case 'an':
        return 'ANDROID';
        break;
      case 'df':
      default:
      	return 'DEFAULT';
      	break;
    }
  }

  public static function getCtrlZoomStr($type)
  {
    switch ($type) {
      case 'sm':
        return 'SMALL';
        break;
      case 'lg':
        return 'LARGE';
        break;
       case 'df':
      default:
      	return 'DEFAULT';
      	break;
    }
  }
  
  public static function validateCoordsExact($coords) {
    return (preg_match('/^([-]{0,1}\d+(\.\d*){0,1},[-]{0,1}\d+(\.\d*){0,1})$/', $coords));
  }
  public static function splitCoords($coords) {
      $varValueArr=null;
      preg_match_all('/^((?P<lat>[-]{0,1}\d+(\.\d*){0,1}),(?P<lng>[-]{0,1}\d+(\.\d*){0,1}))$/', $coords, $varValueArr);

      
      $lat = $varValueArr['lat'][0];
      $lng = $varValueArr['lng'][0];
      return array(
          'cm_lat' => $lat,
          'cm_lng' => $lng
      );
 }
  
/**
 * calculate Distanz between two locations
 * 1. location: lat1,lng1
 * 2. location: coords
 */

  public static function getDistanceFormula($lat1, $lng1)
  {
    if (!is_numeric($lat1) || !is_numeric($lng1))
        return '"-"';

    $lat1 = deg2rad($lat1);
    $lng1 = deg2rad($lng1);
    $sinLat1 = sin($lat1);
    $cosLat1 = cos($lat1);
    $latitude = 'RADIANS(cm_lat)';
    $longitude = 'RADIANS(cm_lng)';

    /*
     $lat1 *=$this->factor;
     $lng1 *=$this->factor;
     $latitude *=$this->factor;
     $longitude *=$this->factor;
     */

    //  $formula= '6371*ACOS(SIN('.$lat1.')*SIN('.$latitude.')+COS('.$lat1.')*COS('.$latitude.')*COS('.$longitude.'-'.$lng1.'))';
    $formula = '6371*ACOS(ROUND(' . $sinLat1 . '*SIN(' . $latitude . ')+(' . $cosLat1 . ')*COS(' . $latitude . ')*COS(' . $longitude . '-(' . $lng1 . ')),10))';

    return $formula;
  }
  
//  public static function getDistanceOptionsHTML($distancevalues,$selectedItem)
  public static function getDistanceOptionsHTML($distancevalues,&$max_dist)
  {
      if (preg_match('/^((?P<v1>\d+),)*\[(?P<std>\d+)\](,(?P<v2>\d+))*$/',
          $distancevalues))
      {
          $varValueArr=null;
          //get default (preselected dropdown options)
          preg_match_all('/\[(?P<std>\d+)\]/', $distancevalues, $varValueArr);
          
          $std_dist = intval($varValueArr['std'][0]);
          
          //get dropdown options
          preg_match_all('/(?P<v1>\d+)/', $distancevalues, $varValueArr);
          
          $distOptions = '';
          $itemFound = false;
          foreach ($varValueArr['v1'] as $value)
          {
              if (intval($value) == $max_dist)
              {
                  $itemFound = true;
              }
          }
          if (!$itemFound)
          {
              $max_dist = $std_dist;
          }
          foreach ($varValueArr['v1'] as $value)
          {
              
              $val = intval($value);
              $distOptions .= '<option value="' . $val . '" ';
              if ($val == $max_dist)
              {
                  $itemFound = true;
                  $distOptions .= ' selected="selected"';
              }
              $distOptions .= '>' . $val . '</option>';
          }
          return $distOptions;
      }
      return null;
  }
  
  public function addGeoCoordsRegexp($strRegexp, $varValue, \Widget $objWidget)
  {
      if ($strRegexp == 'geocoords')
      {
          if (!cm_Map_lib::validateCoordsExact($varValue))
          {
              $objWidget->addError(sprintf($GLOBALS['TL_LANG']['tl_module']['cm_map_coorderr'],$objWidget->label));
          }
          return true;
      }
  }
  
  public function addIntvalsAndDefaultRegexp($strRegexp, $varValue, \Widget $objWidget)
  {
      
      //  die("xxx");
      if ($strRegexp == 'intvalsanddefault')
      {
          if (!preg_match('/^(\d+,)*\[\d+\](,\d+)*$/', $varValue))
          {
              $objWidget->addError(sprintf($GLOBALS['TL_LANG']['tl_module']['cm_map_intvalsanddefaulterr'],$objWidget->label));
          }
          return true;
      }
      return false;
  }
  
  
  public static function getBaseScript($ssl,$language,$api,$apiKey)
  {
      $code=null;
      
      switch ($api)
      {
          case 'GM': $code=($ssl?'https://':'http://').'maps.googleapis.com/maps/api/js'.'?language='.$language.($apiKey?('&key='.$apiKey):'');
          break;
          case 'OSM': $code=($ssl?'https://':'http://').'unpkg.com/leaflet@1.7.1/dist/leaflet.js';
          break;
      }
    return $code;
 	
  }
  public static function getMarkerClusterScript($ssl,$language,$api,$apiKey)
  {
      switch ($api)
      {
          case 'GM': $code=('/system/modules/cm_maps/assets/markerclusterer.js');
          break;
          case 'OSM': $code=($ssl?'https://':'http://').'unpkg.com/leaflet.markercluster@1.5.3/dist/leaflet.markercluster-src.js';
          break;
      }
      
      return $code;
   }
   
   public static function getMarkerClusterCss($ssl,$language,$api,$apiKey)
  {
      switch ($api)
      {
          case 'GM': return null; 
          break;
          case 'OSM': $code = array(
                                ($ssl?'https://':'http://').'unpkg.com/leaflet.markercluster@1.5.3/dist/MarkerCluster.css',
                                ($ssl?'https://':'http://').'unpkg.com/leaflet.markercluster@1.5.3/dist/MarkerCluster.Default.css'
                              );
          break;
      }
      return $code;
  }
  
  public static function getMarkerBaseScript($api,$ssl,$click,$clickType,$stayBubbleOpened=false)
  {
      $code=null;
      switch ($api)
      {
          case 'GM':
              $code ='
  // Function will be executed after page load
  var cm_active_marker=null;
  var cm_active_info=null;
  function getMarker(url,pkt,txt,color,myIcon,dx,dy,map,clickable,isDefault) {
    var info;
    var markerIcon;
    var marker;';
              /*
               .'markerImg = new google.maps.MarkerImage();'
               */
              $code .='
    if (myIcon != "") {
      //var markerImg = new google.maps.MarkerImage(myIcon);
	  var markerImg = {url:myIcon};
    if (dx!=null && dy!=null)
    {
        markerImg = {url: myIcon,
                     anchor: new google.maps.Point(dx, dy)
                    };
    }
    else {
      markerImg = {url: myIcon};
    }
//   var markerShadow = new google.maps.MarkerImage("");
                  
      marker = new google.maps.Marker({
        icon:markerImg,
        position:pkt,
        map:map
      });
    }
    else
    {
      if (color!="") {
        //markerImg = new google.maps.MarkerImage("'.($ssl?'https://':'http://').'www.google.com/intl/en_us/mapfiles/ms/micons/"+color+"-dot.png");
		
		markerImg = {url:"'.($ssl?'https://':'http://').'www.google.com/intl/en_us/mapfiles/ms/micons/"+color+"-dot.png"
					,anchor: new google.maps.Point(16,32)
					}
		
//          markerIcon.iconSize = new GSize(32,32);
        marker = new google.maps.Marker({
          icon:markerImg,
          position:pkt,
          map:map
        });
      }
      else {
        marker = new google.maps.Marker({
          position:pkt,
          map:map
        });
      }
    }
    info = new google.maps.InfoWindow({
        content: txt
    });
            
    if (clickable) {
      google.maps.event.addListener(marker, "'.(($stayBubbleOpened && $clickType==2)?'dblclick':'click').'", function() {
        window.location.href=url
      });
    }';
              
              
              
              if (!$click) {
                  
                  if ($stayBubbleOpened)
                  {
                      $code .='
      google.maps.event.addListener(marker, "'.($clickType==2?'click':'mouseover').'", function() {
        if (cm_active_marker!=null)
        {
          cm_active_info.close(map,cm_active_marker);
        }
        cm_active_info=info;
        cm_active_marker=marker;
        info.open(map,marker);
      });';
                  }
                  else
                  {  $code .='
      google.maps.event.addListener(marker, "mouseover", function() {
        info.open(map,marker);
      });
      google.maps.event.addListener(marker, "mouseout", function() {
        info.close(map,marker);
      });';
                  }
              }
              else {
                  $code .='
    google.maps.event.addListener(marker, "click", function() {
        info.open(map,marker);
    });';
              }
              
              $code .='
  if (isDefault) {
    cm_active_marker=marker;
    cm_active_info=info;
                  
  }
  return marker;
  }';
              break;
case 'OSM':
              $code ='
  // Function will be executed after page load 
  var cm_active_marker=null;
  var cm_active_info=null;

  function getMarker(url,pkt,txt,color,myIcon,dx,dy,p_dx,p_dy,map,clickable,isDefault) {
    var info;
    var markerIcon;
    var marker;';
/*
          .'markerImg = new google.maps.MarkerImage();'
*/
    $code .='
    if (myIcon != "") {
      var markerImg = new L.icon({iconUrl:myIcon
                                 ,iconAnchor:[dx,dy]});
 //                                ,popupAnchor:[p_dx,p_dy]});
if (p_dx && p_dy) {markerImg.popupAnchor=[p_dx,p_dy]}
      //var markerShadow = new L.icon("");

      marker = new L.marker(pkt,{icon: markerImg});
//pqr      marker.addTo(map);  
    }
    else
    {
      if (color!="") {
        markerImg = new L.icon({iconUrl:"'.($ssl?'https://':'http://').'www.google.com/intl/en_us/mapfiles/ms/micons/"+color+"-dot.png"
                                ,iconAnchor:[dx,dy]
                                ,popupAnchor:[p_dx,p_dy]});
//          markerIcon.iconSize = new GSize(32,32);
        marker = new L.marker(pkt,{icon: markerImg});
      }
      else {
        marker = new L.marker(pkt);
      }
//pqr      marker.addTo(map);  
    }

    //info = new L.popup(txt);
    info = marker.bindPopup(txt);
	
    if (clickable) {
  //    L.map.event.addListener(marker, "'.(($stayBubbleOpened && $clickType==2)?'dblclick':'click').'", function() {
        marker.on("'.(($stayBubbleOpened && $clickType==2)?'dblclick':'click').'", function() {
        window.location.href=url
      });
    }';
   
    
if (!$click) {

  if ($stayBubbleOpened)
  {
    $code .='   
      //google.maps.event.addListener(marker, "'.($clickType==2?'click':'mouseover').'", function() {
        marker.on("'.($clickType==2?'click':'mouseover').'", function() {
        if (cm_active_marker!=null)
        {
          //cm_active_info.close(map,cm_active_marker);
           cm_active_info.closePopup(); 
        }
        cm_active_info=info;  
        cm_active_marker=marker;  
        //info.open(map,marker);
        cm_active_info.openPopup();
      });'; 
  }
  else
  {  $code .='   
      //google.maps.event.addListener(marker, "mouseover", function() {
      marker.on("mouseover", function() {
        info.openPopup();
      });
      //google.maps.event.addListener(marker, "mouseout", function() {
      marker.on( "mouseout", function() {
        info.closePopup();
      });';
  }
}
else {
  $code .='   
    //google.maps.event.addListener(marker, "click", function() {
      marker.on("click", function() {
        info.openPopup();;
    });';
}

$code .='
  if (isDefault) {
    cm_active_marker=marker;
    cm_active_info=info;

  }
  return marker;
  }';
          break;
      }
      return $code;
  }

  public static function getGoogleMapsGeoData($location,$ssl,$countryCode='',$gcapi, $gckey='')
  {
    $varValue="";
    $ApiConnection = ApiConnectionFactory::getApiConnector($gcapi);

    $ApiConnection->setParameters('xml', $ssl, $location, $countryCode, $gckey);
    
    if ($ApiConnection->validateResponse())
    {
        //print_r($ApiConnection->getResponse()); die("------>true");
        $lat=$ApiConnection->getLat();
        $lng=$ApiConnection->getLng();
        $varValue = array("lat"=>($lat?$lat->__toString():""), "lng" => ($lng?$lng->__toString():""),"status"=>"OK");
    }
    else
    {
        //print_r($ApiConnection->getResponse()); die("------>false");
        $varValue = array("status"=>"FAILED","response"=>print_r($ApiConnection->getResponse(),true));
    }
    return $varValue;
  }

	public static function getMarkerIcon($icon)
	{

	if (!$icon) return null;
//		if (!is_numeric($icon))
//		{
//		    return $icon;
//		}
	 
//		$objFile = \FilesModel::findByPk($icon);
//		return $objFile->path;
		$iconstd = null;
        $iconTmp = $icon;
//		if (is_numeric($iconTmp = $icon))
//      {
         	$iconTmp = \FilesModel::findByPk($iconTmp);
//			$iconTmp = \FilesModel::findByUuid($iconTmp);
            if ($iconTmp->type == "file")
            {

                $iconstd =  $iconTmp->path;
            }
//       }
		return $iconstd;

	}

}
