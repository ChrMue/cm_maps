<?php
namespace ChrMue\cm_Maps;

class Api_GM extends API_adapter
{
    
    function __construct(Icm_connection $connection)
    {
        $this->apiParameter=new ApiParameter();
        $this->apiParameter->useSSL = true;
        $this->apiParameter->host = 'maps.googleapis.com';
        $this->apiParameter->folder = '/maps/api/geocode';
        $this->apiParameter->options=array();
        $this->setConnection($connection);
    }
       
    /**
     * {@inheritDoc}
     * @see \ChrMue\cm_Maps\API_adapter::getLat()
     */
    public function getLat()
    {
        // TODO Auto-generated method stub
        return $this->xml->result->geometry->location->lat;
    }
    
    /**
     * {@inheritDoc}
     * @see \ChrMue\cm_Maps\API_adapter::getLng()
     */
    public function getLng()
    {
        // TODO Auto-generated method stub
        return $this->xml->result->geometry->location->lng;
    }
    
    /**
     * {@inheritDoc}
     * @see \ChrMue\cm_Maps\API_adapter::requestGeoData()
     */
    public function setParameters($format, $ssl, $location, $countryCode, $key)
    {
        $this->apiParameter->options=array();
        // TODO Auto-generated method stub
        $this->apiParameter->useSSL = ($ssl==true);
        //$this->apiParameter->options['outFormat']=$format;
        $this->apiParameter->subfolder="/".$format;
        $this->apiParameter->options['key']=$key;

        $tmpLocation="";
        $cc = $countryCode;
        $postalCodeValue=null;
        if (is_array($location))
        {
            foreach($location as $param => $value)
            {
                 if ($param=='postalCode' && strlen($value>=4) && ctype_digit($value))
                 {
                     $postalCodeValue=$value;
                 }
                 else
                 {
                     if ($value) 
                     {
                         if (strtolower($param)=="country")  
                         {
                              if (!$countryCode) {
                                  //$this->apiParameter->options["components"]="country:".$value;    
                                  
                                  $cc=$value;
                              }
                          }
                          else
                          {
                              $tmpLocation .= ($tmpLocation=="" ? "" : "+").$value;
                          }
                     }
                 }
            }
        }
        else
        {
            if (strlen($location>=4) && ctype_digit($location))
            {
                $postalCodeValue = $location;
            }
            else 
            {
                $tmpLocation =  $location;
            }
        }
        if ($postalCodeValue)
        {
            //$tmpLocation=strtoupper($cc).'-'.$postalCodeValue.($tmpLocation ? "+".$tmpLocation :'');
            $tmpLocation=$postalCodeValue.($tmpLocation ? "+".$tmpLocation :'');
        }
        if ($cc)
        {
            $this->apiParameter->options["region"]=$cc;
            $tmpLocation = strtoupper($cc).'-'.$tmpLocation;
        }
        if ($postalCodeValue && strlen($postalCodeValue>=4) && ctype_digit($postalCodeValue) && (!is_array($location) || sizeof($location)==1 ))
        {
            $this->apiParameter->options['components']= ($cc ? "country:".$cc."|" : "")."postal_code:".$postalCodeValue;
        }
        else
        {
            $this->apiParameter->options['address']=$tmpLocation;
            
        }
   //     print_r('PARAMETER');
   //     print_r( $this->apiParameter); 
        
   //     print_r('LOACATION');
   //     print_r($location);
   //     print_r('COUNTRY');
   //     print_r($countryCode);
   //      die();
    }
    /**
     * {@inheritDoc}
     * @see \ChrMue\cm_Maps\API_adapter::validateResponse()
     */
    public function validateResponse()
    {
        $this->processRequest();
        //print_r($this->apiParameter);
        //print_r($this->xml); 
        //die('GM'); 
        $this->status = $this->xml!=null && "OK"== $this->xml->status;
        return ($this->status== true);
    }
}
