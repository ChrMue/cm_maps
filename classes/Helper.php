<?php
/**
 * PHP version 5
 * @copyright  Christian Muenster 2009-2020
 * @author     Christian Muenster
 * @package    cm_Maps
 * @license    LGPL
 */

namespace ChrMue\cm_Maps;

class Helper extends \Backend
{
	protected static $strTable = null;

	private function checkTable($fctName)
	{
	    if (static::$strTable!=null && \Database::getInstance()->tableExists(static::$strTable)) return true;
	    
	    $this->log("table ".static::$strTable." not defined" ,$fctName,TL_GENERAL);
	    	//throw new \Exception("table not defined");
	    return false;
	}

    /**
     * @throws \Exception
     * @return NULL[]|mixed[]
     */
    protected function getGCApi($BEonly=false)
    {
        $root_details=null;
        $gcapi=null;
        $gcapiKey=null;
        $isFE=!$BEonly &&  (TL_MODE == 'FE');
        if ($isFE)
        {
            $root_id = \Frontend::getRootPageFromUrl()->id;
            //$root_id = self::getRootPageFromUrl()->id;
            $root_details =  \PageModel::findWithDetails($root_id);
            
            $gcapi = $root_details->cm_gc_api;
        }
        
        if (!$gcapi) { $gcapi=\Config::get('cm_gc_api'); }
        if (!$gcapi) throw new \Exception("Api not selected");
        
        //$gcapiprop="cm_map_apikey_".strtolower($gcapi);
        $gcapiprop=null;
        switch ($gcapi)
        {
            case 'GM': $gcapiprop='cm_map_apikey_gm'; break;
            case 'OSM': $gcapiprop='cm_map_apikey_osm'; break;
        }
        if ($isFE)
        {
            $gcapiKey = $root_details->$gcapiprop;
        }
        if (!$gcapiKey) { $gcapiKey=\Config::get($gcapiprop); }
        
        return array(
            'gcapi'=>$gcapi,
            'gcapikey'=>$gcapiKey
        );
    }
	
    protected function getGeoData($id,$locationData, $oldvalue, $fieldmapping, $api, $apiKey)
    {
        $varValue = $oldvalue;
		if (!$this->checkTable("getGeoData"))return $varValue; 

        $t = static::$strTable;
		$count= $locationData->cm_gc_requestcount;
        //street,postal,city
        if ($locationData)
        {
            $doRefresh = false;
            $doRequest = false;
            // continue if automatic geocoding ist required
            // otherwise leave coordinates unchanged
            if ($locationData->cm_autocoords)
            {
                $locationArr=array();
                $country=null;
                $var =$fieldmapping['country'];
                if ($locationData->$var)
                {
                    $country = $locationData->$var;
                    $locationArr['country']=$locationData->$var;
                }
                $var = $fieldmapping['postal'];
                $location = str_replace(' ', '+', 
                    /*($country ? strtoupper($country).'-' :''). */
                    urlencode($locationData->$var));

                if ($var){$locationArr['postalcode']=str_replace(' ', '+', 
                    /* (($api=='GM' && $country) ? strtoupper($country).'-' :''). */
                    $locationData->$var);}
                $var = $fieldmapping['city'];
                if ($locationData->$var)
                {
                    if ($location) $location .= '+';
                 //   $location .= str_replace(' ', '+', urlencode($locationData->$var));
                    $locationArr['city']=str_replace(' ', '+', $locationData->$var);
                }
                $var =  $fieldmapping['street'];
                if ($locationData ->$var)
                {
                    if ($location) $location .= ',';
                  //  $location .= str_replace(' ', '+', urlencode($locationData->$var));
                    $locationArr['street']=str_replace(' ', '+', $locationData->$var);
                }
                $useSSL = \Config::get('cm_request_gm_ssl') || \Environment::get('ssl');
		$data = cm_Map_lib::getGoogleMapsGeoData($locationArr, $useSSL,$country,$api,$apiKey);
                $status ='unknown';
                if ($data)
                {
                    $lat = $data["lat"];
                    $lng = $data["lng"];
                    $varValue =$lat.','.$lng;
		    $status = $data["status"];
                }
                if ($status =='FAILED')
                {
                    $this->log("Finding location (".$t.") of id " . $locationData->id . ": " . $data["status"], $data["response"], "tl_member update",TL_ERROR);
                }
                else
                {
                    $this->log("Finding location (".$t.") of id " . $locationData->id . ": " . "data: " . $lat . "," . $lng. " (" . $data["status"]  .")", "tl_member update",TL_GENERAL);
                }
                $doRequest=true;
                $doRefresh = ($varValue != $oldvalue || 
                	$locationData->cm_lat != $lat || 
                	$locationData->cm_lng != $lng
		);
		$count++;
            } else
            {
            	$doRefresh = ($varValue != $oldvalue ||
            	($locationData->cm_lat.','.$locationData->cm_lng)!=$varValue);
         		list($lat, $lng, $altitude) = explode(',', $varValue);
		    }
            if ($doRefresh)
            {
            	$arr = array('cm_coords' => $varValue, 
                			 'cm_lat' => $lat, 
                			 'cm_lng' => $lng);
            	\Database::getInstance()->prepare("UPDATE ".$t." %s"
//                 			.($doRequest?",cm_gc_requestcount=cm_gc_requestcount+1":"")
                			." WHERE id=?")->set($arr)->execute($locationData->id);
        		$this->log(sprintf("%s: id %s: %s (Anfrage %s)->%s",
							$t,
							$id,
							$varValue,
							$count,
							$status),
							$t." update",TL_GENERAL
						  );
            }
        }
   	return $varValue;
    }
	
    protected function doGeoRouting($dc)
    {
        if (!$this->checkTable("geoRouting"))return; 

        $t = static::$strTable;
		$fieldmapping =$this->getFieldMapping();

    	$limit = \Config::get('cm_requestlimit');
        if (!$limit) $limit=3;
		$id=null;
		if ($dc && $dc->id)
		{
			$id=$dc->id;
			$msg = ' for parent id='.$id;
		}

		$this->log("Finding locations (".$t.") started".($id?$msg:"")."...", $t." update", TL_GENERAL);
        //    $additionalFields[]=null;

		$objLocation = \Database::getInstance()->prepare('SELECT * FROM '.$t.' WHERE cm_autocoords=1 '
                                .'AND (coalesce(cm_gc_requestcount,0)<='.$limit.') AND (cm_coords="," ' . ' OR cm_coords="" )'
                                .($id?'AND pid='.$id:'')
								)->execute();
        if ($objLocation->numRows)
        {
            $apiSettings=$this->getGCApi();
            $api = $apiSettings['gcapi'];
            $apiKey= $apiSettings['gcapikey'];
            
            while ($objLocation->next())
            {
                $varValue = $this->getGeoData($objLocation->id, $objLocation, $objLocation->cm_coords, $fieldmapping, $api,$apiKey);
            }
        }
		$this->log("Finding locations (".$t.") finished", $t." update", TL_GENERAL);
    }
    
    public function cronGeoRouting(){
        $this->doGeoRouting($dc);
    }

    protected function geoRouting($dc){
        $this->doGeoRouting($dc);
        $this->redirect($this->getreferer());
    }

      /**
   * get memberdata (currently stored in the database)
   * and perform google geocoding
   */
    private function getFieldMappingOfField($key,$mapping,$fields)
    {
        if (key_exists($key,$mapping) && $mapping[$key] && 
            key_exists($mapping[$key],$GLOBALS['TL_DCA'][static::$strTable]['fields']))
        {
            $varValue = $mapping[$key];
            $varValue = \Input::decodeEntities($varValue);
            $varValue = \Input::xssClean($varValue, true);
            $varValue = \Input::stripTags($varValue);
            return $varValue;
        }
        return $key;
    }
	
    private function getFieldMapping()
    {
        $t = static::$strTable;
        $mapping = $GLOBALS['TL_DCA'][$t]['config']['cm_gm_mapping'];

        $street = 'street';
        $postal = 'postal';
        $city = 'city';
        $country ='country';
        $has_mapping=false;

        if ($mapping && is_array($mapping))
        {
            $street=$this->getFieldMappingOfField('street',$mapping,$GLOBALS['TL_DCA'][$t]['fields']);
            $postal=$this->getFieldMappingOfField('postal',$mapping,$GLOBALS['TL_DCA'][$t]['fields']);
            $city=$this->getFieldMappingOfField('city',$mapping,$GLOBALS['TL_DCA'][$t]['fields']);
            $country=$this->getFieldMappingOfField('country',$mapping,$GLOBALS['TL_DCA'][$t]['fields']);
        }
        return array(
            'street' =>$street,
            'postal' =>$postal,
            'city'   =>$city,
            'country'=>$country
        );
    }
    public function storeLatLng($id,$varValue,$lat,$lng)
    {
        if (!cm_Map_lib::validateCoordsExact($varValue)) return;
        $t = static::$strTable;
        $arr = cm_Map_lib::splitCoords($varValue);
        if ($arr['cm_lat']==$lat && $arr['cm_lng']==$lng) return;
        \Database::getInstance()->prepare("UPDATE ".$t." %s"
        // .($doRequest?",cm_gc_requestcount=cm_gc_requestcount+1":"")
            ." WHERE id=?")->set($arr)->execute($id);          
    }
    
    public function getGeoDataBase($varValue, $dc, $api, $key)
    {
        if (!$this->checkTable("getGMGeoDataBE"))return $varValue;
        $t = static::$strTable;
        $fieldmapping =$this->getFieldMapping();
        $street=$fieldmapping['street'];
        $postal=$fieldmapping['postal'];
        $city=$fieldmapping['city'];
        $country=$fieldmapping['country'];
        //street,postal,city
        $objLocation = \Database::getInstance()->prepare("SELECT id,$street,$postal,$city,$country,cm_lat,"
            ."cm_lng,cm_autocoords,cm_gc_requestcount FROM ".$t." WHERE id=?")
            ->limit(1)
            ->execute($dc->id);


        if ($objLocation->numRows)
        {
            $data=$this->getGeoData($objLocation->id,$objLocation,$varValue,$fieldmapping, $api,$key);

            $varValue=$data; //['lat'].','.$data['lng'];
            // otherwise coordinates will be unchanged
        }
        return $varValue;
    }
	
    public function getGeoDataBE($varValue, \DataContainer $dc)
    {
        if (!$dc->activeRecord->cm_autocoords) 
        { 
            $this->storeLatLng($dc->id, $varValue, $dc->activeRecord->cm_lat,$dc->activeRecord->cm_lng);
            return $varValue; 
        }
        $apiSettings=$this->getGCApi(true);
        //print_r($apiSettings); die();
        $api = $apiSettings['gcapi'];
        $apiKey= $apiSettings['gcapikey'];

        return $this->getGeoDataBase($varValue, $dc, $api, $apiKey);
    }

    public function getGeoDataFE($varValue, $dc=null)
    {
        if (TL_MODE == 'BE' || !$dc->cm_autocoords) { return $varValue; }

        $apiSettings=$this->getGCApi(false);
        $api = $apiSettings['gcapi'];
        $apiKey= $apiSettings['gcapikey'];
        return $this->getGeoDataBase($varValue->cm_coords, $varValue, $api, $apiKey);           
    }

    public function resetCounterBE($varValue, $dc)
    {
        $t = static::$strTable;	
        $this->log("Reset request (".$t.") counter".($varValue ? " from ".$varValue : "")." to 0" ,$t." update",TL_GENERAL);
        $varValue = 0;
        return $varValue;
    }
	
    public function resetCounterFE($varValue, $dc)
    {
        if (TL_MODE == 'BE') return $varValue; 
        return $this->resetCounterBE($varValue->cm_gc_requestcount, $dc);
    }   
    
    public static function checkCookieBarInstalled()
    {
        $bundles = \Contao\System::getContainer()->getParameter('kernel.bundles');
		if (isset($bundles['pct_privacy_manager'])) 
        {
            return 2;
        }		
        if (!isset($bundles['ContaoCookiebar'])) return 0;
        if (TL_MODE == 'FE')
        { 
            $root_id = \Frontend::getRootPageFromUrl()->id;
            //$root_id = self::getRootPageFromUrl()->id;
            $root_details = \PageModel::findWithDetails($root_id);
            $retVal = $root_details->activateCookiebar;
            return ($retVal ? 1 : 0);
        }
        return 0;       
    }
}

