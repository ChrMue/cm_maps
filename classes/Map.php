<?php

/**
 * Class Map
 *
 * @copyright  Christian Muenster 2020 
 * @author     Christian Muenster 
 * @package    cm_Map 
 *
 */
namespace ChrMue\cm_Maps;

class Map
{
  public $BaseJsScript;
  public $MainJsScript;
  public $MarkerClusterJsScript;
  public $MarkerClusterCssScript;
  
  public $baseJsTemplate='cm_googlemap_js_base';
  public $mainJsTemplate='cm_googlemap_js_main';
  public $routeDefJsTemplate='cm_googlemap_js_routedef';
  public $routeInitJsTemplate='cm_googlemap_js_routeinit';

  public $language = 'de';

  public $settings=array();

  public $items = array();
  public $markerData = array();
  public $circleData = array();
  public $marker;
  public $template;
  public $singleView;
  public $layout;
  public $api;
  public $cookieId; 
  
  public function __construct($singleView,$api='GM')
  {
      $this->singleView=$singleView;
      $this->api=$api;
      switch ($api)
      {
          case'GM': 
            $this->mainJsTemplate='cm_googlemap_js_main'; 
            break;
          case'OSM': 
            $this->mainJsTemplate='cm_osm_js_main'; 
            break;
      }
  }
  
  public function generate() {

    $this->template= new \Contao\FrontendTemplate($this->mainJsTemplate);

    $detailUrl="";
    $cookieBar = Helper::checkCookieBarInstalled();
    if ($cookieBar>0)
    {  
        switch ($cookieBar)
        {
        case 1: 
            $this->settings['cookiebarInstalled'] = ($this->cookieId && $this->cookieId > 0) ? $cookieBar : 0;
            $this->settings['cookieId'] = $this->cookieId;
            break;
        case 2:    
            $this->settings['cookiebarInstalled'] = $cookieBar;
            $this->settings['cookieId'] = null;
            break;
        default:
            $this->settings['cookiebarInstalled'] = 0;
            $this->settings['cookieId'] = null;
        }
    }
    $this->template->apiScriptUrl = $this->BaseJsScript;
    $this->template->clusterScriptUrl = $this->MarkerClusterJsScript;                     

    $this->template->clusterCssAdditionalHeader = $this->MarkerClusterCssScript;
    global $objPage; 
    $this->template->pageId= $objPage->id;
	
    $this->template->settings=$this->settings;
    
    $this->template->singleView=$this->singleView;

    if (!$this->singleView)
    {
	if ($this->markerData && count($this->markerData)>0)
        {
	    foreach ($this->markerData as $item)
	    {
              if (!cm_Map_lib::validateCoordsExact($item['cm_coords'])) continue;
              $items[] = array(
	        'data' => $item['data'],
	        'infotext' => $item['infotext'],
	        'cm_coords' => $item['cm_coords'],
	        'icon' => $item['icon']['url'],
	        'icon_x' => $item['icon']['icon_anchor']['left'],
	        'icon_y' => $item['icon']['icon_anchor']['top'],
	        'iconpopup_x' => (float)($item['icon']['popup_anchor']['left'] ? $item['icon']['popup_anchor']['left'] : 0),
	        'iconpopup_y' => (float)($item['icon']['popup_anchor']['top'] ? $item['icon']['popup_anchor']['top'] : -intval($item['icon']['icon_anchor']['top'])),
	        'color' => $item['color'],
	        'url' => $item['url'],
	        'clickable' => $item['clickable'],
            'near' => $item['near']
	      );
            }
        }
        $this->template->items=$items;
        if ($this->circleData)
        {
            $this->template->circleData=$this->circleData;
        }
    }
    else
    {
      if ($this->settings['routeToDetail'])
      {
          
//        $tmpl= new \Contao\FrontendTemplate($this->routeDefJsTemplate);
//        $tmpl->mapID = $this->settings['mapID'];
//        $tmpl->coords = $this->settings['lat'].','.$this->settings['lng'];
//        $this->template->routeDefScript=$tmpl->parse();
      }
      $item = array(
        'infotext' => $this->marker['infotext'],
        'cm_coords' => $this->marker['cm_coords'],
        'icon' => $this->marker['icon']['url'],
        'icon_x' => $this->marker['icon']['icon_anchor']['left'] ?? null,
        'icon_y' => $this->marker['icon']['icon_anchor']['top'] ?? null,
        'iconpopup_x' => $this->marker['icon']['popup_anchor']['left'] ?? 0,
        'iconpopup_y' => $this->marker['icon']['popup_anchor']['top'] ?? -intval($this->marker['icon']['icon_anchor']['top'] ?? 0),
        'color' => $this->marker['color'],
        'clickable' => $this->marker['clickable']
      );
	  if ($this->showAdditionalItem && $this->additionalMarker)
	  {
		$additionalItem = array(
            'infotext' => $this->additionalMarker['infotext'],
            'cm_coords' => $this->additionalMarker['cm_coords'],
		    'icon' => $this->additionalMarker['icon']['url'],
		    'icon_x' => $this->additionalMarker['icon']['icon_anchor']['left'] ?? null,
		    'icon_y' => $this->additionalMarker['icon']['icon_anchor']['top'] ?? null,
		    'iconpopup_x' => $this->additionalMarker['icon']['popup_anchor']['left'] ?? 0,
		    'iconpopup_y' => $this->additionalMarker['icon']['popup_anchor']['top'] ?? -intval($this->additionalMarker['icon']['icon_anchor']['top'] ?? 0),
		    'color' => $this->additionalMarker['color'],
            'clickable' => $this->additionalMarker['clickable']
		);
	  }
	        
	  $this->template->item=$item;
	  $this->template->additionalItem=$additionalItem;
    }
	$this->template->showAdditionalItem = $this->showAdditionalItem && $this->additionalMarker;
    $this->template->layout=$this->layout;
  }
       
   public function parse() {
       //    echo $this->template->parse();
     return $this->template->parse();
   }

   public function initCookieBarSettings($details)
   {
       if (Helper::checkCookieBarInstalled()==1)
       {
           $cookiename='';
           switch ($this->api)
           {
               case'GM':
               case'OSM':$cookiename='cm_cookieid_'.strtolower($this->api); break;
           }
           $x = $details->$cookiename;
           $x1= intval($x);
            $this->cookieId = $x1;
           if ($this->cookieId==0)
           {
               $this->cookieId = intval(\Contao\Config::get($cookiename));
               $root_id = \Frontend::getRootPageFromUrl()->id;
               $root_details = \PageModel::findWithDetails($root_id);
               $rootEntry = intval($root_details->$cookiename);
               if ($rootEntry>0)
               {
                   $this->cookieId = $rootEntry;
               }
           }
       }
   }
   

}
