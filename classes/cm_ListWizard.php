<?php


/**
 * Class cm_SortWizard
 *
 * @copyright  Christian Muenster 2020
 * @author     Christian Muenster 
 * @package    cm_Maps 
 *
 */
namespace ChrMue\cm_Maps;


//use Contao\StringUtil;

class cm_ListWizard extends \Widget
{

	/**
	 * Submit user input
	 * @var boolean
	 */
	protected $blnSubmitInput = true;

	/**
	 * Template
	 * @var string
	 */
	protected $strTemplate = 'be_widget';


	/**
	 * Add specific attributes
	 * @param string
	 * @param mixed
	 */
// 	public function __set($strKey, $varValue)
// 	{
// 		switch ($strKey)
// 		{
// 			case 'value':
// 				$this->varValue = \StringUtil::deserialize($varValue);
// 				break;

// 			case 'mandatory':
// 				$this->arrConfiguration['mandatory'] = $varValue ? true : false;
// 				break;

// 			default:
// 				parent::__set($strKey, $varValue);
// 				break;
// 		}
// 	}




	/**
	 * Generate the widget and return it as string
	 * @return string
	 */
	public function generate()
	{
		$this->import('Database');

	//	$arrButtons = array('copy', 'up', 'down', 'delete');
		$arrButtons = array('copy', 'delete', 'drag');		
		$strCommand = 'cmd_' . $this->strField;

		// Change the order
// 		if (\Input::get($strCommand) && is_numeric(\Input::get('cid')) && \Input::get('id') == $this->currentRecord)
// 		{

// 			switch (\Input::get($strCommand))
// 			{
// 				case 'copy':
// 					$this->varValue = array_duplicate($this->varValue, \Input::get('cid'));
// 					break;

// 				case 'up':
// 					$this->varValue = array_move_up($this->varValue, \Input::get('cid'));
// 					break;

// 				case 'down':
// 					$this->varValue = array_move_down($this->varValue, \Input::get('cid'));
// 					break;

// 				case 'delete':
// 					$this->varValue = array_delete($this->varValue, \Input::get('cid'));
// 					break;
// 			}
// 		}
// 		if (\Input::get($strCommand) || \Input::post('FORM_SUBMIT') == $this->strTable)
// 		{
// 			\Database::getInstance()->prepare("UPDATE " . $this->strTable . " SET " . $this->strField . "=? WHERE id=?")
// 						   ->execute(serialize($this->varValue), $this->currentRecord);

// 			if (is_numeric(\Input::get('cid')) && \Input::get('id') == $this->currentRecord)
// 			{
// 			$this->redirect(preg_replace('/&(amp;)?cid=[^&]*/i', '', preg_replace('/&(amp;)?' . preg_quote($strCommand, '/') . '=[^&]*/i', '', \Environment::get('request'))));
// 		}
// 		}
        if (\Input::post('FORM_SUBMIT') == $this->strTable)
        {
            $this->varValue = \Input::post($this->strId);
        }
        // Make sure there is at least an empty array
		if (!is_array($this->varValue) || !$this->varValue[0])
		{
			$this->varValue = array(array(''));
		}

		// Begin table
		$return = '<table id="ctrl_'.$this->strId.'"class="tl_modulewizard" summary="List wizard">
  <thead>
    <tr>
      <th>'.$GLOBALS['TL_LANG']['tl_module']['cm_field'].'</th><th>&nbsp;</th>
    </tr>
  </thead>
  <tbody class="sortable">';
		// Add fields
		for ($i=0; $i<count($this->varValue); $i++)		
		{
		    $options = '';

		    foreach ($this->options as $itemField)
		    {
		        $options .= '<option value="'.\StringUtil::specialchars($itemField['value']).'" '
		            . static::optionSelected($itemField['value'], $this->varValue[$i]['field']) . '>'
		                . $itemField['label'].'</option>';
		    }
		    
		    $return .= '
    <tr>
      <td><select name="'.$this->strId.'[' . $i . '][field]" id="'.$this->strId.'_field_'.$i.'" class="tl_select tl_chosen" onfocus="Backend.getScrollOffset()" onchange="Backend.updateModuleLink(this)">'
		    . $options . '</select></td>';
	
			// Add row buttons
			$return .= '
      <td style="white-space:nowrap; padding-left:3px;">';

			foreach ($arrButtons as $button)
			{
// 			     $return .= '<a href="'.$this->addToUrl('&'.$strCommand.'='.$button.'&cid='.$i.'&id='.$this->currentRecord).'&amp;rt=' . REQUEST_TOKEN 
// 			         .'" title="'.specialchars($GLOBALS['TL_LANG'][$this->strTable]['cm_memberlist_'.$button][0])
// 			         .'" onclick="Backend.moduleWizard(this, \''.$button.'\', \'ctrl_'.$this->strId.'\'); return false;">'
// 			         .$this->generateImage($button.'.gif', $GLOBALS['TL_LANG'][$this->strTable]['cm_memberlist_'.$button][0], 'class="tl_listwizard_img"').'</a> ';
			    if ($button == 'edit')
			    {
			        $return .= ' <a href="contao/main.php?do=themes&amp;table=tl_module&amp;act=edit&amp;id=' . $this->varValue[$i]['mod'] . '&amp;popup=1&amp;nb=1&amp;rt=' . REQUEST_TOKEN . '" title="' . \StringUtil::specialchars($GLOBALS['TL_LANG']['tl_layout']['edit_module']) . '" class="module_link" ' . (($this->varValue[$i]['mod'] > 0) ? '' : ' style="display:none"') . ' onclick="Backend.openModalIframe({\'title\':\'' . \StringUtil::specialchars(str_replace("'", "\\'", $GLOBALS['TL_LANG']['tl_layout']['edit_module'])) . '\',\'url\':this.href});return false">' . \Image::getHtml('edit.svg') . '</a>' . \Image::getHtml('edit_.svg', '', 'class="module_image"' . (($this->varValue[$i]['mod'] > 0) ? ' style="display:none"' : ''));
			    }
			    elseif ($button == 'drag')
			    {
			        $return .= ' <button type="button" class="drag-handle" title="' . \StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['move']) . '" aria-hidden="true">' . \Image::getHtml('drag.svg') . '</button>';
			    }
			    elseif ($button == 'enable')
			    {
			        $return .= ' <button type="button" data-command="enable" class="mw_enable" title="' . \StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['mw_enable']) . '">' . \Image::getHtml((($this->varValue[$i]['enable']) ? 'visible.svg' : 'invisible.svg')) . '</button><input name="' . $this->strId . '[' . $i . '][enable]" type="checkbox" class="tl_checkbox mw_enable" value="1" onfocus="Backend.getScrollOffset()"' . (($this->varValue[$i]['enable']) ? ' checked' : '') . '>';
			    }
			    else
			    {
			        $return .= ' <button type="button" data-command="' . $button . '" title="' . \StringUtil::specialchars($GLOBALS['TL_LANG']['MSC']['mw_' . $button]) . '">' . \Image::getHtml($button . '.svg') . '</button>';
			    }
			}

			$return .= '</td>
    </tr>';
		}
		return $return.'
  </tbody>
  </table>
  <script>Backend.moduleWizard("ctrl_' . $this->strId . '")</script>';

	}
}
