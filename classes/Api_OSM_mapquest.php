<?php
namespace ChrMue\cm_Maps;

class Api_OSM_mapquest extends API_adapter
{
    
    function __construct(Icm_connection $connection)
    {
        $this->apiParameter=new ApiParameter();
        $this->apiParameter->useSSL = true;
        $this->apiParameter->host = 'www.mapquestapi.com';
        $this->apiParameter->folder = '/geocoding/v1/address';
        $this->apiParameter->options=array();
        $this->setConnection($connection);
    }

    
    /**
     * {@inheritDoc}
     * @see \ChrMue\cm_Maps\API_adapter::getLat()
     */
    public function getLat()
    {
        // TODO Auto-generated method stub
		$result= $this->xml->results->result;
		if (is_array($result)) $result = $result[0];
        return $result->locations->location[0]->latLng->lat;
    }
    
    /**
     * {@inheritDoc}
     * @see \ChrMue\cm_Maps\API_adapter::getLng()
     */
    public function getLng()
    {
        // TODO Auto-generated method stub
		$result= $this->xml->results->result;
		if (is_array($result)) $result = $result[0];
        return $result->locations->location[0]->latLng->lng;
    }
        
    /**
     * {@inheritDoc}
     * @see \ChrMue\cm_Maps\API_adapter::requestGeoData()
     */
    public function setParameters($format, $ssl, $location, $countryCode, $key)
    {
        $this->apiParameter->options=array();
        // TODO Auto-generated method stub
        $this->apiParameter->useSSL = ($ssl==true);
        $this->apiParameter->options['outFormat']=$format;
        $this->apiParameter->options['key']=$key;
		$addCountry=$countryCode ? true : false;
        if (is_array($location))
        {    
            foreach($location as $param => $value)
            {
                if ($value) {
                  if (strtolower($param)=="country")  
                  {
					  $addCountry=false;
                  }
                $this->apiParameter->options[$param]=$value;
                }
            }
        }
        else{
            $this->apiParameter->options['location']=$location;
        }            
		if ($addCountry) $this->apiParameter->options['location'].=','.$countryCode;
    }
   /**
     * {@inheritDoc}
     * @see \ChrMue\cm_Maps\API_adapter::validateResponse()
     */
    public function validateResponse()
    {
        $this->processRequest();
        //print_r($this->apiParameter);
        //print_r($this->xml); echo ('OSM');//die('OSM');
        $this->status = $this->xml!=null && "0"== $this->xml->info->statusCode;
		//echo "status:".($this->status==true?"Ja":"nein");
        return ($this->status== true);
    }   
}
