<?php


namespace ChrMue\cm_Maps;

class cm_LatLngAjax extends \backend {
		
public function storePickedCoords($strAction, \DataContainer $dc)
{
    if ($strAction == 'cm_addGeoCoord')
    {

$text = 'Ajax storePickedCoords()<br>';
$text .= 'action '.$strAction.'<br>';
$text .= 'act '.\Input::get('act').'<br>';

$text .= 'value '.\Input::post('value').'<br>';

$this->log('cm_LatLngAjax: cm_addGeoCoord', $text, TL_GENERAL);
				$intId = \Input::get('id');
				$strField = $strFieldName = \Input::post('name');

				// Handle the keys in "edit multiple" mode
				if (\Input::get('act') == 'editAll')
				{
					$intId = preg_replace('/.*_([0-9a-zA-Z]+)$/', '$1', $strField);
					$strField = preg_replace('/(.*)_[0-9a-zA-Z]+$/', '$1', $strField);
				}

				// The field does not exist
				if (!isset($GLOBALS['TL_DCA'][$dc->table]['fields'][$strField]))
				{
					$this->log('Field "' . $strField . '" does not exist in DCA "' . $dc->table . '"', 'Ajax executePostActions()', TL_ERROR);
					header('HTTP/1.1 400 Bad Request');
					die('Bad Request');
				}

				$varValue = \Input::post('value', true);
/*
				$strKey = ($this->strAction == 'reloadPagetree') ? 'pageTree' : 'fileTree';

				// Convert the selected values
				if ($varValue != '')
				{
					$varValue = trimsplit("\t", $varValue);

					// Automatically add resources to the DBAFS
					if ($strKey == 'fileTree')
					{
						foreach ($varValue as $k=>$v)
						{
							$varValue[$k] = \Dbafs::addResource($v)->id;
						}
					}

					$varValue = serialize($varValue);
				}
*/
				$strKey = 'cm_LatLng';

				// Set the new value
				if ($GLOBALS['TL_DCA'][$dc->table]['config']['dataContainer'] == 'File')
				{
					$GLOBALS['TL_CONFIG'][$strField] = $varValue;
					$arrAttribs['activeRecord'] = null;
				}
				elseif ($intId > 0 && $this->Database->tableExists($dc->table))
				{
					$objRow = $this->Database->prepare("SELECT * FROM " . $dc->table . " WHERE id=?")
											 ->execute($intId);

					// The record does not exist
					if ($objRow->numRows < 1)
					{
						$this->log('A record with the ID "' . $intId . '" does not exist in table "' . $dc->table . '"', 'Ajax executePostActions()', TL_ERROR);
						header('HTTP/1.1 400 Bad Request');
						die('Bad Request');
					}

					$objRow->$strField = $varValue;
					$arrAttribs['activeRecord'] = $objRow;
				}

				$arrAttribs['id'] = $strFieldName;
				$arrAttribs['name'] = $strFieldName;
				$arrAttribs['value'] = $varValue;
				$arrAttribs['strTable'] = $dc->table;
				$arrAttribs['strField'] = $strField;


						
				$objWidget = new $GLOBALS['BE_FFL'][$strKey]($arrAttribs);
				echo $objWidget->generate();
				exit; //break; 
    }
}

}
